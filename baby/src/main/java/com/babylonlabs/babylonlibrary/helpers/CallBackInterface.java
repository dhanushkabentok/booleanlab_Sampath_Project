package com.babylonlabs.babylonlibrary.helpers;

import android.graphics.Bitmap;

import com.babylonlabs.babylonlibrary.network.model.Response.BiometricConfigResponse;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;

/**
 * Created by dhanushka on 1/16/18.
 */

public class CallBackInterface {

    public interface userLogingStatesCallback {

        void onSuccessuserLogingStatesCallback(int response,boolean logingste);

        void onSuccessBiometricsCompletedCallback(String response);

        void onFaileduserLogingStatesCallback(String message);
    }


    public interface RegisterbiometricCallback {

        void onSuccessRegisterbiometricCallback(RegisterBiometricResponse response);

        void onFailedRegisterbiometricCallback(String  message);
    }

    public interface FaceRegisterbiometricCallback {

        void onSuccessFaceRegisterbiometricCallback(boolean response);

        void onFailedFaceRegisterbiometricCallback(boolean  message);
    }


    public interface FaceRegisterbiometricLoginCallback {

        void onSuccessFaceRegisterbiometricLoginCallback(boolean response);

        void onFailedFaceRegisterbiometricLoginCallback(boolean  message);
    }

    public interface FaceRegisterbiometricLoginNotificationCallbackCallback {

        void onSuccessFaceRegisterbiometricLoginNotificationCallbackCallback(boolean response);

        void onFailedFaceRegisterbiometricLoginNotificationCallbackCallback(boolean  message);
    }

    public interface FaceLoginbiometricLoginCallback {

        void onSuccessLoginbiometricLoginCallback(boolean response);

        void onFailedFLoginbiometricLoginCallback(boolean  message);
    }
    public interface FaceLoginbiometricLoginmCallbackNetworkNotificationCallback {

        void onSuccessLoginbiometricLoginmCallbackNetworkNotificationCallback(boolean response);

        void onFailedFLoginbiometricLoginmCallbackNetworkNotificationCallback(boolean  message);
    }

    public interface userLogingCallback {

        void onSuccessSeverLogingCallback(BiometricConfigResponse mBiometricConfigResponse);

        void onFailedSeverLogingCallback(String message);
    }

    public interface biometricSucsessCallback {

        void onSuccessSeverLogingCallback(int response);

        void onFailedSeverLogingCallback(int message);

        void onSuccessSeverPINingCallback(int message);
    }

    public interface biometricPINSucsessCallback {

        void onSuccessSeverPINLogingCallback(int response);

        void onFailedSeverPINLogingCallback(int message);
    }

    public interface biometricPINSeverCallback {

        void onSuccessSeverPINSeverCallback(int response);

        void onFailedSeverPINSeverCallback(String message);
    }

    public interface DialogCallbackInterface {
        void onDialogConfirmed(int s);
    }

    public interface MessegBoxCallbackInterface {
        void onMessegBoxDialogConfirmed(int s);
    }

    public interface UpdateBiometricCallbackInterface {
        void onDialogBiometricConfirmed(int i, String e);
    }

    public interface DialogBiometricCallbackInterface {
        void onDialogBiometricConfirmed(int s);
    }

    public interface RegisterbiometricFaceCallback {

        void onSuccessRegisterbiometricFaceCallback(int i, String text);
        void onFailedRegisterbiometricFaceCallback(String message);
        void onCaptureImageFaceCallback(String Buttonmessage, int testmesseg, String imageName, Bitmap mBitmap);
    }
}

