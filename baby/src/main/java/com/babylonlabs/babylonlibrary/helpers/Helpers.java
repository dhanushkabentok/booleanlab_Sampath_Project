package com.babylonlabs.babylonlibrary.helpers;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.ViewClass.MainAuthenticateClass;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricConfigResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.google.gson.Gson;


/**
 * Created by dhanushka on 16/10/2017.
 */

public class Helpers {


    /**
     * Check EditText is empty?
     *
     * @param editText
     * @param message
     * @return
     */

    private static Dialog mProgress;

    public static boolean isEmptyEditText(EditText editText, String message) {

        if (editText.getText().toString().trim().isEmpty()) {
            editText.setError(message);
            editText.requestFocus();
            return true;
        }
        return false;
    }

    public static void saveToPreferences(Context context, UserSetup user, boolean userLoggedInState) {

        final SharedPreferences preferences = context.getSharedPreferences(
                BabylonConstant.PREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();

        try {
            editor.putString(BabylonConstant.PREF_KEY_ACCESS_TOKEN, user.getUserToken());
            editor.putString(BabylonConstant.PREF_KEY_Device_ID, user.getDeviceId());
            editor.putBoolean(BabylonConstant.PREF_KEY_FINGERPRINT, user.isFingerprint());
            editor.putBoolean(BabylonConstant.PREF_KEY_USER_LOGGED_IN_STATE, userLoggedInState);
            Gson gson = new Gson();
            String userJson = gson.toJson(user);
            editor.putString(BabylonConstant.PREF_KEY_USER, userJson);
            editor.commit();

            editor.apply();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static String getUser(Context context) {

        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(BabylonConstant.PREF_KEY_USER, "");
    }

    public static void saveUserToken(Context context, String userToken) {

        final SharedPreferences preferences = context.getSharedPreferences(
                BabylonConstant.PREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();

        editor.putString(BabylonConstant.USER_TOKEN, userToken);
        editor.commit();
    }

    public static String getUserToken(Context context) {

        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(BabylonConstant.USER_TOKEN, "");
    }


    public static boolean getUserLoggedInState(Context context) {
        final SharedPreferences prefs = getPreferences(context);

        return prefs.getBoolean(BabylonConstant.PREF_KEY_USER_LOGGED_IN_STATE, false);
    }

    public static String getAccessToken(Context context) {
        final SharedPreferences prefs = getPreferences(context);

        return prefs.getString(BabylonConstant.PREF_KEY_ACCESS_TOKEN, "");
    }

    public static boolean getAccessFingerpront(Context context) {

        final SharedPreferences prefs = getPreferences(context);
        return prefs.getBoolean(BabylonConstant.PREF_KEY_FINGERPRINT, false);
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(BabylonConstant.PREFERENCES, Context.MODE_PRIVATE);
    }

    @SuppressLint("ApplySharedPref")
    public static void removePreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(BabylonConstant.PREFERENCES, Context.MODE_PRIVATE);

        preferences.edit().remove(BabylonConstant.PREF_KEY_ACCESS_TOKEN).commit();
        preferences.edit().remove(BabylonConstant.PREF_KEY_FINGERPRINT).commit();
        preferences.edit().remove(BabylonConstant.PREF_KEY_LOCATION).commit();
        preferences.edit().remove(BabylonConstant.PREF_KEY_USER).commit();

        preferences.edit().putBoolean(
                BabylonConstant.PREF_KEY_USER_LOGGED_IN_STATE, false
        ).commit();

    }

    public static void removeUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(BabylonConstant.PREFERENCES, Context.MODE_PRIVATE);

        preferences.edit().remove(BabylonConstant.PREF_KEY_USER).commit();
    }


    public static void showProgress(Context mContext) {
        if (mProgress == null) {
            mProgress = new Dialog(mContext, R.style.Progressbar);
            mProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgress.setContentView(R.layout.widget_progress_spinner);
            mProgress.setCancelable(false);
        }

        if (mProgress.isShowing() == false) {
            mProgress.show();
        }
    }

    public static void dismissProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    public static void dilogMessegBox(Context context, final CallBackInterface.MessegBoxCallbackInterface callback, String mBiometricConfigResponse) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_messeg);
        dialog.setCancelable(false);
        dialog.setTitle(null);
        // set the custom dialog components - text, image and button
        Button btYes = (Button) dialog.findViewById(R.id.bt_ok);
        TextView texteorro = (TextView) dialog.findViewById(R.id.texteorro);
        texteorro.setText(mBiometricConfigResponse);

        // if button is clicked, close the custom dialog
        btYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onMessegBoxDialogConfirmed(1);
            }
        });

        dialog.show();
    }


    public static void dilogBox(Context context, final CallBackInterface.DialogCallbackInterface callback) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom);
        dialog.setCancelable(false);
        dialog.setTitle(null);
        // set the custom dialog components - text, image and button
        TextView btYes = (TextView) dialog.findViewById(R.id.bt_yes);
        TextView btNo = (TextView) dialog.findViewById(R.id.bt_no);
        TextView btDontdo = (TextView) dialog.findViewById(R.id.bt_dShow);

        // if button is clicked, close the custom dialog
        btYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onDialogConfirmed(1);
            }
        });

        btNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onDialogConfirmed(2);
            }
        });

        btDontdo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onDialogConfirmed(3);
            }
        });

        dialog.show();
    }


    public static void DilogBoxBiometric(Context context, final CallBackInterface.DialogBiometricCallbackInterface callback, boolean fringerStx) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_biometric);
        dialog.setTitle(null);
        dialog.setCancelable(false);

        // set the custom dialog components - text, image and button
        TextView btFace = (TextView) dialog.findViewById(R.id.bt_face);
        TextView btFinger = (TextView) dialog.findViewById(R.id.bt_finger);
        if (fringerStx) {
            btFinger.setVisibility(View.VISIBLE);
        } else {
            btFinger.setVisibility(View.GONE);
        }
        // if button is clicked, close the custom dialog
        btFinger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onDialogBiometricConfirmed(1);
            }
        });

        btFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callback.onDialogBiometricConfirmed(2);
            }
        });

        dialog.show();
    }
}
