package com.babylonlabs.babylonlibrary.helpers;

/**
 * Created by dhanushka on 20/11/2017.
 */

public class BabylonConstant {

    public static String device_type = "ANDROID";
    public static String device_push_token = "12345";
    public static final String PREFERENCES = "preferences";
    public static final String PREF_KEY_ACCESS_TOKEN = "accesstoken";
    public static final String PREF_KEY_Device_ID = "DeviceId";
    public static final String PREF_KEY_USER_LOGGED_IN_STATE = "user_logged_in_state";
    public static final String PREF_KEY_USER = "user";
    public static final String PREF_KEY_FINGERPRINT = "fingerprint";
    public static final String PREF_KEY_LOCATION = "Location";
    public static final String PREF_FROM_QUOTES = "from_quotes";
    public static final String PREF_DATE_FORMAT = "dd/MM/yyyy";
    public static final String BAYO_YTPE = "payo_type";
    public static final String LOGIN_STATS = "login stats";
    public static final String USER_TOKEN = "bank token";
    public static final String BASE_URL = "https://babylon4c.bentok.com/mobile/";
//    public static final String BASE_URL = "http://192.168.8.100:8000/mobile/";
}
