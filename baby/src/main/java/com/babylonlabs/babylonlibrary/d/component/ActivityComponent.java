package com.babylonlabs.babylonlibrary.d.component;



import com.babylonlabs.babylonlibrary.d.PerActivity;
import com.babylonlabs.babylonlibrary.d.module.ActivityModule;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

}
