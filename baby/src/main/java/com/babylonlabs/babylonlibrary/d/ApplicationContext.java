package com.babylonlabs.babylonlibrary.d;

import javax.inject.Qualifier;

/**
 * Created by dhanushka on 10/10/2017.
 */
@Qualifier
public @interface ApplicationContext {
}
