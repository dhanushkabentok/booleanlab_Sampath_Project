package com.babylonlabs.babylonlibrary.d.component;


import com.babylonlabs.babylonlibrary.d.ApplicationScope;
import com.babylonlabs.babylonlibrary.d.module.ActivityModule;
import com.babylonlabs.babylonlibrary.d.module.ApplicarionServicemodule;
import com.babylonlabs.babylonlibrary.d.module.NetworkAccessModule;
import com.babylonlabs.babylonlibrary.d.module.PicassoModule;
import com.babylonlabs.babylonlibrary.d.module.SicModul;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.ApiService;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.squareup.picasso.Picasso;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@ApplicationScope
@Component(modules = {ApplicarionServicemodule.class, PicassoModule.class, ActivityModule.class, SicModul.class, NetworkAccessModule.class})
public interface ApplicationComponent {

    Picasso getPicasso();
    ApiService getApplicatonService();
    CallApi getClasesink();
    NetworkAccess getNetworkAccess();
}
