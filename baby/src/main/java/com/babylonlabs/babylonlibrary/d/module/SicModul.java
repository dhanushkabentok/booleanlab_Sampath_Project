package com.babylonlabs.babylonlibrary.d.module;


import android.content.Context;

import com.babylonlabs.babylonlibrary.d.ApplicationContext;
import com.babylonlabs.babylonlibrary.d.ApplicationScope;
import com.babylonlabs.babylonlibrary.network.ApiService;
import com.babylonlabs.babylonlibrary.network.CallApi;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dhanushka on 24/10/2017.
 */
@Module(includes = {ContextModule.class, ApplicarionServicemodule.class})
public class SicModul {

    @Provides
    @ApplicationScope
    public CallApi mCallApi(@ApplicationContext Context context, ApiService mApiService) {
        return new CallApi(context, mApiService);
    }
}
