package com.babylonlabs.babylonlibrary.d.module;


import android.content.Context;

import com.babylonlabs.babylonlibrary.d.ApplicationContext;
import com.babylonlabs.babylonlibrary.d.ApplicationScope;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dhanushka on 24/10/2017.
 */
@Module(includes = {ContextModule.class})
public class NetworkAccessModule {

    @Provides
    @ApplicationScope
    public NetworkAccess mNetworkAccess(@ApplicationContext Context context) {
        return new NetworkAccess(context);
    }
}
