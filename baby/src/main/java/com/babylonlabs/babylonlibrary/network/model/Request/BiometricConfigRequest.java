package com.babylonlabs.babylonlibrary.network.model.Request;

/**
 * Created by dhanushka on 1/23/18.
 */

public class BiometricConfigRequest {

    private String product_id;
    private String user_token;
    private Double lat;
    private Double lon;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }


}
