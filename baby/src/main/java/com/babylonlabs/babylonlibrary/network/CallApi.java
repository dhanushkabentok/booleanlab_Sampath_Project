package com.babylonlabs.babylonlibrary.network;


import android.content.Context;

import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.model.Request.AuthenticateUserRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.BiometricConfigRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.BiometricsStetestRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.PinRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricConfigResponse;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricsStetestResponse;
import com.babylonlabs.babylonlibrary.network.model.Response.FaceLoginbiometricLogin;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.User;

import java.util.HashMap;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dhanushka on 19/10/2017.
 */

public class CallApi {

    @Inject
    ApiService mSicModul;
    private Context mContext;


    public CallApi(Context context, ApiService m) {
        mContext = context;
        mSicModul = m;
    }


    public void updateBiometricsStetest(NetworkAccess mNetworkAccess, BiometricsStetestRequest mBiometricsStetestRequest) {

        if (!mNetworkAccess.isNetworkAvailable()) {


            return;
        }

        mSicModul.updateBiometricsStetest(mBiometricsStetestRequest).enqueue(new Callback<BiometricsStetestResponse>() {
            @Override
            public void onResponse(Call<BiometricsStetestResponse> call, Response<BiometricsStetestResponse> response) {


            }

            @Override
            public void onFailure(Call<BiometricsStetestResponse> call, Throwable t) {


            }
        });

    }

    public void getBiometricConfigCall(final CallBackInterface.userLogingCallback callback, NetworkAccess mNetworkAccess, BiometricConfigRequest mBiometricConfigRequest) {

        if (!mNetworkAccess.isNetworkAvailable()) {

            callback.onFailedSeverLogingCallback(null);
            return;
        }

        mSicModul.BiometricConfigRequest(mBiometricConfigRequest.getUser_token(), mBiometricConfigRequest.getLat(), mBiometricConfigRequest.getLon(), mBiometricConfigRequest.getProduct_id()).enqueue(new Callback<BiometricConfigResponse>() {
            @Override
            public void onResponse(Call<BiometricConfigResponse> call, Response<BiometricConfigResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccessSeverLogingCallback(response.body());
                } else {
                    callback.onFailedSeverLogingCallback(response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<BiometricConfigResponse> call, Throwable t) {

                callback.onFailedSeverLogingCallback(t.getMessage());
            }
        });

    }


    public void registerbiometric(final CallBackInterface.RegisterbiometricCallback callback, NetworkAccess mNetworkAccess, RegisterBiometricRequest mRegisterBiometricRequest, int biometricId) {

        if (!mNetworkAccess.isNetworkAvailable()) {

            callback.onFailedRegisterbiometricCallback(null);
            return;
        }

        mSicModul.registerbiometric(biometricId, mRegisterBiometricRequest).enqueue(new Callback<RegisterBiometricResponse>() {
            @Override
            public void onResponse(Call<RegisterBiometricResponse> call, Response<RegisterBiometricResponse> response) {

                if (response.isSuccessful()) {
                    if (!response.body().getStatus().equalsIgnoreCase("error")) {
                        callback.onSuccessRegisterbiometricCallback(response.body());
                    } else {
                        callback.onFailedRegisterbiometricCallback(response.body().getMessage());
                    }

                } else {
                    callback.onFailedRegisterbiometricCallback(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterBiometricResponse> call, Throwable t) {

                callback.onFailedRegisterbiometricCallback(t.getMessage());
            }
        });

    }


    public void registerbiometricFace(final CallBackInterface.RegisterbiometricCallback callback, NetworkAccess mNetworkAccess,
                                      int biometricId, HashMap<String, RequestBody> map, MultipartBody.Part bodyFaceImage, MultipartBody.Part bodyLeftFaceImage, MultipartBody.Part bodyRightFaceImage) {

        if (!mNetworkAccess.isNetworkAvailable()) {

            callback.onFailedRegisterbiometricCallback(null);
            return;
        }

        mSicModul.registerbiometricFace(biometricId, map, bodyFaceImage, bodyLeftFaceImage, bodyRightFaceImage).enqueue(new Callback<RegisterBiometricResponse>() {
            @Override
            public void onResponse(Call<RegisterBiometricResponse> call, Response<RegisterBiometricResponse> response) {

                if (response.isSuccessful()) {
                    if (!response.body().getStatus().equalsIgnoreCase("error")) {
                        callback.onSuccessRegisterbiometricCallback(response.body());
                    } else {
                        callback.onFailedRegisterbiometricCallback(response.body().getMessage());
                    }

                } else {
                    callback.onFailedRegisterbiometricCallback(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterBiometricResponse> call, Throwable t) {

                callback.onFailedRegisterbiometricCallback(t.getMessage());
            }
        });

    }


    public void AuthenticateFINGER(NetworkAccess mNetworkAccess, AuthenticateUserRequest mAuthenticateUserRequest, int mUpdateBiometricStatus) {

        if (!mNetworkAccess.isNetworkAvailable()) {
            return;
        }

        mSicModul.AuthenticateFINGER(mAuthenticateUserRequest, mUpdateBiometricStatus).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    public void AuthenticateFINGERNotificTION(NetworkAccess mNetworkAccess, AuthenticateUserRequest mAuthenticateUserRequest, int mUpdateBiometricStatus) {

        if (!mNetworkAccess.isNetworkAvailable()) {
            return;
        }

        mSicModul.AuthenticateFINGER(mAuthenticateUserRequest, mUpdateBiometricStatus).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }

    public void AuthenticateFace(NetworkAccess mNetworkAccess, HashMap<String, RequestBody> map, int bodyFaceImage, final CallBackInterface.FaceLoginbiometricLoginCallback mUpdateBiometricStatus, MultipartBody.Part MultipartbodyFaceImage) {

        if (!mNetworkAccess.isNetworkAvailable()) {
            return;
        }

        mSicModul.AuthenticateFace(map, bodyFaceImage, MultipartbodyFaceImage).enqueue(new Callback<FaceLoginbiometricLogin>() {
            @Override
            public void onResponse(Call<FaceLoginbiometricLogin> call, Response<FaceLoginbiometricLogin> response) {

                if (response.isSuccessful()) {

                    if (!response.body().getStatus().equalsIgnoreCase("error")) {
                        mUpdateBiometricStatus.onSuccessLoginbiometricLoginCallback(true);

                    } else {
                        mUpdateBiometricStatus.onFailedFLoginbiometricLoginCallback(false);

                    }
                } else {

                    mUpdateBiometricStatus.onFailedFLoginbiometricLoginCallback(false);

                }
            }

            @Override
            public void onFailure(Call<FaceLoginbiometricLogin> call, Throwable t) {
                mUpdateBiometricStatus.onFailedFLoginbiometricLoginCallback(false);
            }
        });

    }

    public void AuthenticateFaceNotification(NetworkAccess mNetworkAccess, HashMap<String, RequestBody> map, int bodyFaceImage, final CallBackInterface.FaceLoginbiometricLoginmCallbackNetworkNotificationCallback mUpdateBiometricStatus, MultipartBody.Part MultipartbodyFaceImage) {

        if (!mNetworkAccess.isNetworkAvailable()) {
            return;
        }

        mSicModul.AuthenticateFaceNotification(map, bodyFaceImage, MultipartbodyFaceImage).enqueue(new Callback<FaceLoginbiometricLogin>() {
            @Override
            public void onResponse(Call<FaceLoginbiometricLogin> call, Response<FaceLoginbiometricLogin> response) {

                if (response.isSuccessful()) {
                    if (Integer.getInteger(response.body().getCode()) > 0) {
                        mUpdateBiometricStatus.onSuccessLoginbiometricLoginmCallbackNetworkNotificationCallback(true);

                    } else {
                        mUpdateBiometricStatus.onFailedFLoginbiometricLoginmCallbackNetworkNotificationCallback(false);

                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<FaceLoginbiometricLogin> call, Throwable t) {
                mUpdateBiometricStatus.onFailedFLoginbiometricLoginmCallbackNetworkNotificationCallback(false);
            }
        });

    }



    public void registerbiometricPIN(final CallBackInterface.biometricPINSeverCallback callback, NetworkAccess mNetworkAccess, PinRequest mPinRequest) {

        if (!mNetworkAccess.isNetworkAvailable()) {

            callback.onFailedSeverPINSeverCallback(null);
            return;
        }

        mSicModul.registerbiometricPIN( mPinRequest).enqueue(new Callback<RegisterBiometricResponse>() {
            @Override
            public void onResponse(Call<RegisterBiometricResponse> call, Response<RegisterBiometricResponse> response) {

                if (response.isSuccessful()) {
                    if (!response.body().getStatus().equalsIgnoreCase("error")) {
                        callback.onSuccessSeverPINSeverCallback(1);
                    } else {
                        callback.onFailedSeverPINSeverCallback(response.body().getMessage());
                    }

                } else {
                    callback.onFailedSeverPINSeverCallback(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterBiometricResponse> call, Throwable t) {

                callback.onFailedSeverPINSeverCallback(t.getMessage());
            }
        });

    }

    public void logingPIN(final CallBackInterface.biometricPINSeverCallback callback, NetworkAccess mNetworkAccess, PinRequest mPinRequest) {

        if (!mNetworkAccess.isNetworkAvailable()) {

            callback.onFailedSeverPINSeverCallback(null);
            return;
        }

        mSicModul.logingPIN( mPinRequest).enqueue(new Callback<RegisterBiometricResponse>() {
            @Override
            public void onResponse(Call<RegisterBiometricResponse> call, Response<RegisterBiometricResponse> response) {

                if (response.isSuccessful()) {
                    if (!response.body().getStatus().equalsIgnoreCase("error")) {
                        callback.onSuccessSeverPINSeverCallback(1);
                    } else {
                        callback.onFailedSeverPINSeverCallback(response.body().getMessage());
                    }

                } else {
                    callback.onFailedSeverPINSeverCallback(response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<RegisterBiometricResponse> call, Throwable t) {

                callback.onFailedSeverPINSeverCallback(t.getMessage());
            }
        });

    }



}
