package com.babylonlabs.babylonlibrary.network.model;

/**
 * Created by dhanushka on 1/9/18.
 */

public class UpdateBiometricStatus {

    String user_token;

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;
}
