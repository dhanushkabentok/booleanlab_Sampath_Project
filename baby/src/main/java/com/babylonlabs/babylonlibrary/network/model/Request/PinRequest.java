package com.babylonlabs.babylonlibrary.network.model.Request;

/**
 * Created by dhanushka on 3/11/18.
 */

public class PinRequest {

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    String user_token;
    String pin;
}
