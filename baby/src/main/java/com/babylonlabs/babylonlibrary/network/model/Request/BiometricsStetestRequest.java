package com.babylonlabs.babylonlibrary.network.model.Request;

/**
 * Created by dhanushka on 1/23/18.
 */

public class BiometricsStetestRequest {
    String user_token;
    String biometric_status;

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getBiometric_status() {
        return biometric_status;
    }

    public void setBiometric_status(String biometric_status) {
        this.biometric_status = biometric_status;
    }


}
