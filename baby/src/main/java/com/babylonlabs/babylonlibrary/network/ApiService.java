package com.babylonlabs.babylonlibrary.network;


import com.babylonlabs.babylonlibrary.network.model.Request.AuthenticateUserRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.BiometricsStetestRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.PinRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricConfigResponse;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricsStetestResponse;
import com.babylonlabs.babylonlibrary.network.model.Response.FaceLoginbiometricLogin;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.UpdateBiometricStatus;
import com.babylonlabs.babylonlibrary.network.model.User;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by dhanushka on 10/10/2017.
 */

public interface ApiService {

    @POST("biometric_config/")
    Call<BiometricsStetestResponse> updateBiometricsStetest(@Body BiometricsStetestRequest mBiometricsStetestRequest);

    @POST("register_biometric/{biometric_id}/")
    Call<RegisterBiometricResponse> registerbiometric(@Path("biometric_id") int biometricId, @Body RegisterBiometricRequest mRegisterBiometricRequest);

    @Multipart
    @POST("register_biometric/{biometric_id}/")
    Call<RegisterBiometricResponse> registerbiometricFace(@Path("biometric_id") int biometricId, @PartMap() Map<String, RequestBody> partMap,@Part MultipartBody.Part MultipartbodyFaceImage,@Part MultipartBody.Part bodyLeftFaceImage,@Part MultipartBody.Part bodyRightFaceImage);

    @POST("users/{username}/")
    Call<User> uPdateBiometricStatus(@Body UpdateBiometricStatus mUpdateBiometricStatus);

    @Multipart
    @POST("authenticate/{biometric_id}/")
    Call<FaceLoginbiometricLogin> AuthenticateFace(@PartMap() Map<String, RequestBody> partMap, @Path("biometric_id") int biometricId, @Part MultipartBody.Part MultipartbodyFaceImage);


    @Multipart
    @POST("authenticate/{biometric_id}/")
    Call<FaceLoginbiometricLogin> AuthenticateFaceNotification(@PartMap() Map<String, RequestBody> partMap, @Path("biometric_id") int biometricId, @Part MultipartBody.Part MultipartbodyFaceImage);



    @POST("authenticate/{biometric_id}/")
    Call<User>  AuthenticateFINGER(@Body AuthenticateUserRequest mUpdateBiometricStatus, @Path("biometric_id") int biometricId);

    @POST("register_pin/")
    Call<RegisterBiometricResponse>  registerbiometricPIN(@Body PinRequest mUpdateBiometricStatus);

    @POST("authenticate_pin/")
    Call<RegisterBiometricResponse>  logingPIN(@Body PinRequest mUpdateBiometricStatus);

    @GET("biometric_config/")
    Call<BiometricConfigResponse> BiometricConfigRequest(@Query("user_token") String user_token, @Query("lat") Double lat, @Query("lon") Double lon, @Query("product_id") String product_id);
}
