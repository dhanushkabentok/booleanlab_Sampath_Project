package com.babylonlabs.babylonlibrary.network.model.Request;

/**
 * Created by dhanushka on 1/24/18.
 */

public class AuthenticateUserRequest {

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    String user_token;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    String channel;
}
