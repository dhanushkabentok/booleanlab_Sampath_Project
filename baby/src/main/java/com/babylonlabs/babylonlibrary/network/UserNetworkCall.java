package com.babylonlabs.babylonlibrary.network;


import android.content.Context;

import com.babylonlabs.babylonlibrary.d.ApplicationContext;
import com.babylonlabs.babylonlibrary.d.ApplicationScope;
import com.babylonlabs.babylonlibrary.d.module.ContextModule;

import dagger.Module;
import dagger.Provides;

/**
 * Created by dhanushka on 12/10/2017.
 */
@Module(includes = {ContextModule.class})
public class UserNetworkCall {


    @Provides
    @ApplicationScope
    public Context apiService(@ApplicationContext Context context) {

        return context;
    }


}
