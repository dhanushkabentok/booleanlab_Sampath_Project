package com.babylonlabs.babylonlibrary.network.model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dhanushka on 1/23/18.
 */

public class BiometricConfigResponse {

    @SerializedName("status")
    @Expose
    private String status;

    /* = 0 if biometric not configured,
       = 1 if on configered and enabled for the given product,
       = -1 if dissabled (don't show),
       = -3 if biometrics are disabled on this product,*/
    @SerializedName("biometric_status")
    @Expose
    private String biometric_status;

    /*<biometric_id> *this field is only available on biometric_status =1*/

    @SerializedName("biometric_id")
    @Expose
    private String biometric_id;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("code")
    @Expose
    private int code;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBiometric_status() {
        return biometric_status;
    }

    public void setBiometric_status(String biometric_status) {
        this.biometric_status = biometric_status;
    }

    public String getBiometric_id() {
        return biometric_id;
    }

    public void setBiometric_id(String biometric_id) {
        this.biometric_id = biometric_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }



}
