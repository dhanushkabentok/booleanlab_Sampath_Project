package com.babylonlabs.babylonlibrary.ViewClass;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;

/**
 * Created by whit3hawks on 11/16/16.
 */
@SuppressLint("NewApi")
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;
    CallBackInterface.UpdateBiometricCallbackInterface mBiometricCallback;

    // Constructor
    public FingerprintHandler(Context mContext, CallBackInterface.UpdateBiometricCallbackInterface mCallBacl) {
        context = mContext;
        this.mBiometricCallback = mCallBacl;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        mBiometricCallback.onDialogBiometricConfirmed(3, "Fingerprint Authentication error");
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {

        mBiometricCallback.onDialogBiometricConfirmed(1, "Fingerprint Authentication help");
    }

    @Override
    public void onAuthenticationFailed() {

        mBiometricCallback.onDialogBiometricConfirmed(1, "Fingerprint Authentication failed.");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        mBiometricCallback.onDialogBiometricConfirmed(2, "");

    }

}
