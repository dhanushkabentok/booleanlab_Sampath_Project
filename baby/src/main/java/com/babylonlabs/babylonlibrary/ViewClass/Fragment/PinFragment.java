package com.babylonlabs.babylonlibrary.ViewClass.Fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.PinRequest;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.chaos.view.PinView;
import com.google.gson.Gson;

import javax.inject.Inject;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by dhanushka on 3/11/18.
 */

@SuppressLint("ValidFragment")
public class PinFragment extends Fragment implements CallBackInterface.biometricPINSeverCallback {
    ApplicationComponent mComponent;
    private Context mcntx;
    boolean newUser;
    CallBackInterface.biometricPINSucsessCallback mBiometricSucsess;
    @Inject
    public CallApi mDataManager;

    @Inject
    public NetworkAccess mNetworkAccess;

    @SuppressLint("ValidFragment")
    public PinFragment(CallBackInterface.biometricPINSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats) {
        this.mBiometricSucsess = mCallback;
        this.mComponent = component;
        this.mDataManager = component.getClasesink();
        this.mNetworkAccess = component.getNetworkAccess();
        this.mcntx = mContext;
        this.newUser = stats;
    }

    public static PinFragment PinFragmentint(CallBackInterface.biometricPINSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats) {
        return new PinFragment(mCallback, component, mContext, stats);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pin_fragment, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {

        final PinView pin = (PinView) view.findViewById(R.id.firstPinView);
        Button ok = (Button ) view.findViewById(R.id.ok);
        pin.setAnimationEnable(true);

        pin.setLineColor(ResourcesCompat.getColor(getResources(), R.color.button_couler, getActivity().getTheme()));

        pin.setLineColor(ResourcesCompat.getColorStateList(getResources(), R.color.red, getActivity().getTheme()));

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!newUser){
                    registerPin(pin.getText().toString());
                }else {
                    LogingPin(pin.getText().toString());
                }
            }

        });

    }

    private void LogingPin(String pin) {

        PinRequest mPinRequest = new PinRequest();
        mPinRequest.setPin(pin);
        mPinRequest.setUser_token(Helpers.getUserToken(getActivity()));
        Helpers.showProgress(mcntx);
        mDataManager.logingPIN(this, mNetworkAccess, mPinRequest);
    }

    private void registerPin(String pin) {

        PinRequest mPinRequest = new PinRequest();
        mPinRequest.setPin(pin);
        mPinRequest.setUser_token(Helpers.getUserToken(getActivity()));
        Helpers.showProgress(mcntx);
        mDataManager.registerbiometricPIN(this, mNetworkAccess, mPinRequest);

    }


    @Override
    public void onSuccessSeverPINSeverCallback(int response) {
        Helpers.dismissProgress();
        mBiometricSucsess.onSuccessSeverPINLogingCallback(1);
    }

    @Override
    public void onFailedSeverPINSeverCallback(String message) {
        Helpers.dismissProgress();
        mBiometricSucsess.onFailedSeverPINLogingCallback(0);
    }
}