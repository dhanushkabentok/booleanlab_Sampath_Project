package com.babylonlabs.babylonlibrary.ViewClass;

import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;

import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.BiometricConfigRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.BiometricsStetestRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.BiometricConfigResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.google.gson.Gson;

import javax.inject.Inject;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by dhanushka on 1/16/18.
 */

public class MainAuthenticateClass implements CallBackInterface.userLogingCallback, CallBackInterface.DialogCallbackInterface, CallBackInterface.DialogBiometricCallbackInterface, CallBackInterface.MessegBoxCallbackInterface {

    private String userToken;
    private Context mcntx;
    KeyguardManager keyguardManager;
    FingerprintManager fingerprintManager;

    CallBackInterface.userLogingStatesCallback Callback;
    CallBackInterface.userLogingCallback mUserLogingCallback = this;
    CallBackInterface.DialogCallbackInterface mDialogCallback = this;

    @Inject
    public CallApi mDataManager;

    @Inject
    public NetworkAccess mNetworkAccess;

    public MainAuthenticateClass(Context mContext, String token, ApplicationComponent component, CallBackInterface.userLogingStatesCallback mCallBack) {

        this.userToken = token;
        this.mcntx = mContext;
        this.mDataManager = component.getClasesink();
        this.mNetworkAccess = component.getNetworkAccess();
        this.Callback = mCallBack;

        if (Helpers.getAccessFingerpront(mContext)) {
            keyguardManager = (KeyguardManager) mContext.getSystemService(KEYGUARD_SERVICE);
            fingerprintManager = (FingerprintManager) mContext.getSystemService(FINGERPRINT_SERVICE);
        }


        Helpers.showProgress(mContext);

        HanushekFunction();
    }

    public static MainAuthenticateClass newInstance(Context mContext, String token, ApplicationComponent component, CallBackInterface.userLogingStatesCallback mCallBack) {
        return new MainAuthenticateClass(mContext, token, component, mCallBack);
    }

    private void HanushekFunction() {
        // Will do some task
        Helpers.showProgress(mcntx);
        Helpers.saveUserToken(mcntx, userToken);
        AuthenticationCheckNewtworkCall();
    }


    private void AuthenticationCheckNewtworkCall() {
        /*Call Network Check Loging states  */

        Gson gson = new Gson();
        UserSetup mUserSetup = gson.fromJson(Helpers.getUser(mcntx), UserSetup.class);
        mUserSetup.getDeviceId();

        BiometricConfigRequest mBiometricConfigRequest = new BiometricConfigRequest();
        mBiometricConfigRequest.setLat(mUserSetup.getmLocation().getLatitude());
        mBiometricConfigRequest.setLon(mUserSetup.getmLocation().getLongitude());
        mBiometricConfigRequest.setProduct_id("1");
        mBiometricConfigRequest.setUser_token(userToken);
        mDataManager.getBiometricConfigCall(mUserLogingCallback, mNetworkAccess, mBiometricConfigRequest);
    }

    /*<biometric status
       = 0 if biometric not configured,
       = 1 if on configered and enabled for the given product,
       = -1 if dissabled (don't show),>
       */

    @Override
    public void onSuccessSeverLogingCallback(BiometricConfigResponse response) {
        Helpers.dismissProgress();
        if (response.getStatus().equalsIgnoreCase("success")) {
            int states = Integer.valueOf(response.getBiometric_status());

            switch (states) {

                case 1:
                    //code to be executed;
                    if (response.getBiometric_id() != null) {

                        biometricsLogng(Integer.valueOf(response.getBiometric_id()));
                    } else {
                        biometricsLogng(2);
                    }

                    break;
                case -1:
                    //don't show
                    Helpers.removePreferences(mcntx);
                    Callback.onSuccessBiometricsCompletedCallback("home page");

                    break;
                case -3:
                    //don't show
                    Helpers.removePreferences(mcntx);
                    Callback.onSuccessBiometricsCompletedCallback("home page");

                    break;
                case 0:
                    //new user
                    Helpers.dilogBox(mcntx, (CallBackInterface.DialogCallbackInterface) this);

                    break;
                default:
            }
        }

    }

    private void biometricsLogng(int biometricsId) {
        if (Helpers.getAccessFingerpront(mcntx)) {
            Callback.onSuccessuserLogingStatesCallback(biometricsId, true);
        } else {
            Callback.onFaileduserLogingStatesCallback("Loging Faile ");
        }
    }

    @Override
    public void onFailedSeverLogingCallback(String message) {
        Helpers.dismissProgress();

        Callback.onFaileduserLogingStatesCallback("Loging Faile ");
    }

    @Override
    public void onDialogConfirmed(int s) {

         /*yes = 1 , no = 2 , don't show = 3;*/
        switch (s) {
            case 1:
                //code to be executed;
                Helpers.DilogBoxBiometric(mcntx, this, Helpers.getAccessFingerpront(mcntx));
                break;

            case 2:
                //no
                Callback.onSuccessBiometricsCompletedCallback("home page");

                break;

            case 3:
                //don't show
                Callback.onSuccessBiometricsCompletedCallback("home page");
                BiometricsStetestRequest mBiometricsStetestRequest = new BiometricsStetestRequest();
                mBiometricsStetestRequest.setBiometric_status("3");
                mBiometricsStetestRequest.setUser_token(userToken);
                mDataManager.updateBiometricsStetest(mNetworkAccess, mBiometricsStetestRequest);
                Callback.onSuccessBiometricsCompletedCallback("home page");
                Helpers.removePreferences(mcntx);
                break;

            default:
        }
    }

    @Override
    public void onDialogBiometricConfirmed(int s) {
        if (s == 1) {
            //FingerPrint
            Callback.onSuccessuserLogingStatesCallback(1, false);
        } else {
            Callback.onSuccessuserLogingStatesCallback(2, false);
        }
    }

    @Override
    public void onMessegBoxDialogConfirmed(int s) {
        if (s == 1) {
            Callback.onSuccessBiometricsCompletedCallback("home page");
        } else {
        }
    }
}
