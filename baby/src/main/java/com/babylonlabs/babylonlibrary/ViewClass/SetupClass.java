package com.babylonlabs.babylonlibrary.ViewClass;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.babylonlabs.babylonlibrary.Face.android.AspectRatioTextureView;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.helpers.BabylonConstant;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import static android.content.Context.FINGERPRINT_SERVICE;

/**
 * Created by dhanushka on 1/16/18.
 */

public class SetupClass extends Activity implements LocationListener {

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 10101;
    private String pushToken;
    private Context mcntx;
    boolean fingerprint;
    Location loc;
    LocationManager locationManager;
    private FusedLocationProviderClient mFusedLocationClient;
    String unique_id;

    @SuppressLint("HardwareIds")
    public SetupClass(Context mContext, String token) {
        this.pushToken = token;
        this.mcntx = mContext;
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        loc = new Location("UserLocation");
        loc.setLatitude(0.0);
        loc.setLongitude(0.0);

        unique_id = Settings.Secure.getString(mcntx.getContentResolver(), Settings.Secure.ANDROID_ID);
        int adb = Settings.Secure.getInt(mcntx.getContentResolver(), Settings.Secure.ADB_ENABLED, 0);
//        If it is enabled, adb ==1, otherwise adb == 0.

        CameraUtils _camUtils = new CameraUtils();
//        _camUtils.init();
        getLocation();
    }


    public static SetupClass newInstance(Context mContext, String token) {
        return new SetupClass(mContext, token);
    }

    //get User Curent Location
    void getLocation() {



        CheckFingerprint();

    }



    private void locationGetUser() {

        try {

            locationManager = (LocationManager) mcntx.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
            SaveDataPreferences();
        } catch (SecurityException e) {
            e.printStackTrace();
            SaveDataPreferences();
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        try {

            loc.setLatitude(location.getLatitude());
            loc.setLongitude(location.getLongitude());

            CheckFingerprint();
        } catch (Exception e) {
            SaveDataPreferences();
        }

    }


    public void CheckFingerprint() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) mcntx.getSystemService(FINGERPRINT_SERVICE);

            if (!fingerprintManager.isHardwareDetected()) {
                /**
                 * An error message will be displayed if the device does not contain the fingerprint hardware.
                 * However if you plan to implement a default authentication method,
                 * you can redirect the user to a default authentication activity from here.
                 * Example:
                 * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
                 * startActivity(intent);
                 */
                fingerprint = false;
                SaveDataPreferences();
            } else {

                fingerprint = true;
                SaveDataPreferences();
            }
        } else {
            fingerprint = false;
            SaveDataPreferences();
        }
    }

    private void SaveDataPreferences() {

        UserSetup mUserSetup = new UserSetup();
        mUserSetup.setDeviceId(unique_id);
        Log.d("unique_id2", unique_id);
        mUserSetup.setFingerprint(fingerprint);
        mUserSetup.setUserToken(pushToken);
        mUserSetup.setDeviceType(BabylonConstant.device_type);
        mUserSetup.setmLocation(loc);

        Helpers.saveToPreferences(mcntx, mUserSetup, true);
    }


    @Override
    public void onProviderDisabled(String provider) {

        Toast.makeText(mcntx, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
        checkGPS();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    public void checkGPS() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mcntx);
        // set title
        alertDialogBuilder.setTitle("Enable GPS");
        // set dialog message
        alertDialogBuilder
                .setMessage("Please Enable GPS and Internet!")
                .setCancelable(false)

                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                        getLocation();
                        dialog.dismiss();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
}


