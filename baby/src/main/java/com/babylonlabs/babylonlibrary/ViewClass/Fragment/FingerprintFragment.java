package com.babylonlabs.babylonlibrary.ViewClass.Fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.ViewClass.FingerprintHandler;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.AuthenticateUserRequest;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.inject.Inject;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by dhanushka on 1/16/18.
 */

@SuppressLint("ValidFragment")
public class FingerprintFragment extends Fragment implements CallBackInterface.UpdateBiometricCallbackInterface, CallBackInterface.RegisterbiometricCallback, CallBackInterface.MessegBoxCallbackInterface {

    private KeyStore keyStore;
    FingerprintManager fingerprintManager;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    KeyguardManager keyguardManager;
    CallBackInterface.biometricSucsessCallback mBiometricSucsess;
    TextView desc;
    boolean newUser , notification;
    ApplicationComponent mComponent;
    private Context mcntx;
    @Inject
    public CallApi mDataManager;

    @Inject
    public NetworkAccess mNetworkAccess;

    @SuppressLint("ValidFragment")
    public FingerprintFragment(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats,  boolean notification) {
        this.mBiometricSucsess = mCallback;
        this.mComponent = component;
        this.mDataManager = component.getClasesink();
        this.mNetworkAccess = component.getNetworkAccess();
        this.mcntx = mContext;
        this.newUser = stats;
        this.notification = notification;
    }

    public static FingerprintFragment newInstance(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats,boolean notification) {
        return new FingerprintFragment(mCallback, component, mContext, stats, notification);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fringerfrint, container, false);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        desc = (TextView) view.findViewById(R.id.desc);
        ImageView  icon = (ImageView) view.findViewById(R.id.icon);

        Glide.with(getActivity())
                .load(R.drawable.fingerprint_animation)
                .into(icon);

        keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
        FingerPrintConfigure();
    }

    @SuppressLint("NewApi")
    private void FingerPrintConfigure() {

        if (!fingerprintManager.isHardwareDetected()) {
            /**
             * An error message will be displayed if the device does not contain the fingerprint hardware.
             * However if you plan to implement a default authentication method,
             * you can redirect the user to a default authentication activity from here.
             * Example:
             * Intent intent = new Intent(this, DefaultAuthenticationActivity.class);
             * startActivity(intent);
             */

            desc.setText("Your Device does not have a Fingerprint Sensor");

        } else {

            // Checks whether fingerprint permission is set on manifest
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                desc.setText("Fingerprint authentication permission not enabled");
            } else {

                // Check whether at least one fingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    DilogBox();
                    desc.setText("Register at least one fingerprint in Settings");
//                    startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));

                } else {

                    // Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {
                        desc.setText("Lock screen security not enabled in Settings");
                        DilogBox();

                    } else {
                        fingerprintcheck();
                    }
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void fingerprintcheck() {

        generateKey();
        if (cipherInit()) {

            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
            FingerprintHandler helper = new FingerprintHandler(getActivity(), this);
            helper.startAuth(fingerprintManager, cryptoObject);
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {

        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {

            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();

        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    @Override
    public void onDialogBiometricConfirmed(int i, String e) {

        if (i == 1) {

            desc.setText("invalid login");

        } else if (i == 2) {
            if (newUser) {
                if(!notification) {
                    logingbiometric();
                } else {
                    loginNotification();
                }

            } else {
                registerbiometric();
            }

        } else {
            if (newUser) {
                mBiometricSucsess.onSuccessSeverPINingCallback(1);
            } else {
                mBiometricSucsess.onFailedSeverLogingCallback(0);
            }
        }
    }

    private void loginNotification() {
        AuthenticateUserRequest mAuthenticateUserRequest = new AuthenticateUserRequest();
        mAuthenticateUserRequest.setUser_token(Helpers.getUserToken(mcntx));
        mAuthenticateUserRequest.setChannel(Helpers.getUserToken(mcntx));
        mDataManager.AuthenticateFINGERNotificTION(mNetworkAccess, mAuthenticateUserRequest, 1);

        mBiometricSucsess.onSuccessSeverLogingCallback(1);
    }

    private void logingbiometric() {
        //login success we will update sever

        AuthenticateUserRequest mAuthenticateUserRequest = new AuthenticateUserRequest();
        mAuthenticateUserRequest.setUser_token(Helpers.getUserToken(mcntx));
        mDataManager.AuthenticateFINGER(mNetworkAccess, mAuthenticateUserRequest, 1);
        mBiometricSucsess.onSuccessSeverLogingCallback(1);

    }

    private void registerbiometric() {

        /*Call Network Check registerbiometric states */

        Gson gson = new Gson();
        UserSetup mUserSetup = gson.fromJson(Helpers.getUser(mcntx), UserSetup.class);
        mUserSetup.getDeviceId();

        RegisterBiometricRequest mRegisterBiometricRequest = new RegisterBiometricRequest();
        mRegisterBiometricRequest.setDevice_key(mUserSetup.getDeviceId());
        mRegisterBiometricRequest.setDevice_type("Android");
        mRegisterBiometricRequest.setLocation("all");
        mRegisterBiometricRequest.setProduct_ids("1");
        mRegisterBiometricRequest.setPush_token(mUserSetup.getUserToken());
        mRegisterBiometricRequest.setUser_token(Helpers.getUserToken(mcntx));
        Helpers.showProgress(mcntx);

        mDataManager.registerbiometric(this, mNetworkAccess, mRegisterBiometricRequest, 1);

    }

    @Override
    public void onSuccessRegisterbiometricCallback(RegisterBiometricResponse response) {
        Helpers.dismissProgress();

//        mBiometricSucsess.onSuccessSeverLogingCallback(1);
        mBiometricSucsess.onSuccessSeverPINingCallback(1);
    }

    @Override
    public void onFailedRegisterbiometricCallback(String message) {
        Helpers.dismissProgress();
        Helpers.dilogMessegBox(mcntx, (CallBackInterface.MessegBoxCallbackInterface) this, message);
    }

    @Override
    public void onMessegBoxDialogConfirmed(int s) {
        mBiometricSucsess.onFailedSeverLogingCallback(0);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("dhanushkakumara", "onActivityResult");

        FingerPrintConfigure();
    }

    public void DilogBox() {

        final Dialog dialog = new Dialog(mcntx);
        dialog.setContentView(R.layout.custom_box);
        dialog.setTitle(null);
        dialog.setCancelable(false);

        // set the custom dialog components - text, image and button
        TextView btFace = (TextView) dialog.findViewById(R.id.bt_face);
        TextView btFinger = (TextView) dialog.findViewById(R.id.bt_finger);


        btFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS), 0);


            }
        });

        dialog.show();
    }

}
