package com.babylonlabs.babylonlibrary.Face;

public class BRFState {

	public static final int FACE_DETECTION			= 0;//"state_face_detection";
	public static final int FACE_TRACKING_START		= 1;//"state_face_tracking_start";
	public static final int FACE_TRACKING			= 2;//"state_face_tracking";
	public static final int RESET					= 3;//"state_reset";
 }
