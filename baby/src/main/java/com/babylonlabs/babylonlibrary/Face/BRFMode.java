package com.babylonlabs.babylonlibrary.Face;

public class BRFMode {
	public static final int FACE_DETECTION 	= 0x00001;//"mode_face_detection";
	public static final int FACE_LANDMARKS 	= 0x00011;
	public static final int BLINK_DETECTION = 0x00111;
	public static final int POSE_DETECTION	= 0x01011;
//	public static final int FACE_DETECTION = 0;//"mode_face_detection";
//	public static final int FACE_TRACKING  = 1;//"mode_face_tracking";
//	public static final int POINT_TRACKING = 2;//"mode_point_tracking";
}
