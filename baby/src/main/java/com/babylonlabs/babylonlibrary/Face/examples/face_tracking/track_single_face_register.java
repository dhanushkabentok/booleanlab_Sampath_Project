package com.babylonlabs.babylonlibrary.Face.examples.face_tracking;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;

import com.babylonlabs.babylonlibrary.Face.BRFFace;
import com.babylonlabs.babylonlibrary.Face.BRFManager;
import com.babylonlabs.babylonlibrary.Face.BRFMode;
import com.babylonlabs.babylonlibrary.Face.BRFState;
import com.babylonlabs.babylonlibrary.Face.BlinkCallback;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.Face.android.DrawingUtils;
import com.babylonlabs.babylonlibrary.Face.examples.BRFBasicJavaExample;
import com.babylonlabs.babylonlibrary.Face.geom.Point;
import com.babylonlabs.babylonlibrary.Face.geom.Rectangle;
import com.babylonlabs.babylonlibrary.Face.utils.FileUtils;
import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.google.gson.Gson;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by dhanushka on 2/16/18.
 */

public class track_single_face_register extends BRFBasicJavaExample implements BlinkCallback {
    Context mContext;
    CallBackInterface.RegisterbiometricFaceCallback mRegisterbiometric;
    double LeftEyeX;
    double RightEyeX;
    double eyeDist;
    int eyeYDist;
    boolean skip = true;


    public track_single_face_register(Context context, CallBackInterface.RegisterbiometricFaceCallback mFaceRegisterbiometric) {
        super(context);
        this.mContext = context;
        this.funtFaceImage = false;
        this.leftFaceImage = false;
        this.rightFaceImage = false;
        this.mRegisterbiometric = mFaceRegisterbiometric;
    }


    @Override
    public void onBlink(long eyeCloseDuration, long eyeOpenDuration) {
        Log.d("Blink", "Blinking Time:" + eyeCloseDuration + " Eyes openTime:" + eyeOpenDuration);
    }


    @Override
    public void initCurrentExample(BRFManager brfManager, Rectangle resolution) {

        eyeDist = resolution.width * 0.25;
        eyeYDist = (int) (resolution.width * 0.6 + 1);

        RightEyeX = 0.375 * resolution.width + 1;
        LeftEyeX = RightEyeX + eyeDist;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getContext().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

        brfManager.init(resolution, resolution, _appId);
        brfManager.setMode(BRFMode.POSE_DETECTION | BRFMode.BLINK_DETECTION);
        brfManager.setFaceDetectionParams(0.7, 0.8);
        brfManager.setBlinkDetectionParams(0.6, 0.2, this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void updateCurrentExample(BRFManager brfManager, Bitmap imageData, DrawingUtils draw) {

        if (skip = false) {
            return;
        } else {
            skip = false;
        }

        brfManager.update(imageData);
        draw.clear();

        Vector<BRFFace> faces = brfManager.getFaces();

        if (faces.size() > 0) {

            detecFace(faces, draw, imageData);

        } else {

            mRegisterbiometric.onFailedRegisterbiometricFaceCallback("Can not detection your face");
        }

    }

    @SuppressLint("SetTextI18n")
    private void detecFace(Vector<BRFFace> faces, DrawingUtils draw, final Bitmap imageData) {


        for (int i = 0; i < faces.size(); i++) {

            BRFFace face = faces.get(i);

            if (face.state == BRFState.FACE_TRACKING_START || face.state == BRFState.FACE_TRACKING) {

                draw.drawTriangles(face.vertices, face.triangles, false, 1.0, 0xffffff, 0.2);
                draw.drawVertices(face.vertices, 5.0, false, 0xffffff, 0.3);

                double imageDist = getEyeDist(getLeftEye(face.vertices), getRightEye(face.vertices));
                mRegisterbiometric.onFailedRegisterbiometricFaceCallback("Get closer to the camera - Face too far to recognise");

                if (eyeDist - 20 < imageDist && imageDist < eyeDist + 20) {
                    mRegisterbiometric.onSuccessRegisterbiometricFaceCallback(1, "Take Front Face Image");

                    if (!funtFaceImage) {
                        mRegisterbiometric.onSuccessRegisterbiometricFaceCallback(0, "Take Front Face Image");

                        if (0.0 < face.rotationY && face.rotationY > 0.11) {

                            mRegisterbiometric.onCaptureImageFaceCallback("Take Front Face Image", 1, "FaceImage", imageData);
                        }

                        return;
                    } else if (!leftFaceImage) {

                        if (-30.0 < face.rotationY && face.rotationY < -11.7) {

                            mRegisterbiometric.onCaptureImageFaceCallback("Take Left Face Image", 2, "leftFaceImage", imageData);

                        }
                        return;
                    } else if (!rightFaceImage) {

                        if (11.0 < face.rotationY && face.rotationY < 50.7) {

                            mRegisterbiometric.onCaptureImageFaceCallback("Take  Right Face Image", 3, "rightFaceImage", imageData);

                            return;
                        }

                    }

                } else {

                    mRegisterbiometric.onFailedRegisterbiometricFaceCallback("Get closer to the camera - Face too far to recognise");
                }
            }
        }
    }


    Point getRightEye(float[] vertices) {
        return new Point((vertices[36 * 2] + vertices[39 * 2]) / 2, (vertices[36 * 2 + 1] + vertices[39 * 2] + 1) / 2);
    }


    Point getLeftEye(float[] vertices) {
        return new Point((vertices[42 * 2] + vertices[45 * 2]) / 2, (vertices[42 * 2 + 1] + vertices[45 * 2] + 1) / 2);
    }

    double getEyeDist(Point left, Point right) {
        double d1 = left.x - right.x;
        double d2 = left.y - right.y;
        return Math.sqrt(d1 * d1 + d2 * d2);
    }


}
