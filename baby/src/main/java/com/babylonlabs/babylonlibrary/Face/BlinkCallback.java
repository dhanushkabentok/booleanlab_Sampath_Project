package com.babylonlabs.babylonlibrary.Face;

public interface BlinkCallback {
    void onBlink(long eyeCloseDuration, long eyeOpenDuration);
}
