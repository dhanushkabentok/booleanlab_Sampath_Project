package com.babylonlabs.babylonlibrary.Face.viewFace;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.Face.android.AspectRatioTextureView;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.Face.examples.BRFBasicJavaExample;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.track_single_face;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.track_single_faceNotification;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.track_single_face_register;
import com.babylonlabs.babylonlibrary.Face.geom.Rectangle;
import com.babylonlabs.babylonlibrary.Face.utils.FileUtils;
import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.helpers.RoundedCornersTransformation;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.Vector;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by dhanushka on 2/21/18.
 */

@SuppressLint("ValidFragment")
public class BRFv4FragmentNofit extends Fragment implements Constrect.userLogingStatesCallback, CallBackInterface.FaceRegisterbiometricCallback, CallBackInterface.RegisterbiometricCallback, CallBackInterface.FaceRegisterbiometricLoginNotificationCallbackCallback {

    TextView idVuew;
    FrameLayout camaraview;
    private CameraUtils _camUtils;
    private AspectRatioTextureView _preview;
    private Bitmap _previewImageData;
    private BRFBasicJavaExample _example;
    private TextView _textView;
    ImageView image1, image2, image3, corect1, corect2, corect3;
    TextView  image2_face, textView2, textView;
    private boolean login = true;
    RelativeLayout imageViewdd;
    CallBackInterface.biometricSucsessCallback mBiometricSucsess;
    TextView desc;
    boolean newUser;
    String bNotification;
    boolean button;

    private Context mcntx;
    Button camara_buttton, button_tecimage, uplode_buttton;
    RelativeLayout camara_view;
    ApplicationComponent mComponent;
    Uri myUriFaceImage, myUrileftFaceImage, myUrirightFaceImage;
    File bitmapFile, leftFaceImage, rightFaceImage;
    @Inject
    public CallApi mDataManager;
    Picasso mPicasso;
    @Inject
    public NetworkAccess mNetworkAccess;

    @SuppressLint("ValidFragment")
    public BRFv4FragmentNofit(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, String bNotification) {
        this.mBiometricSucsess = mCallback;
        this.mComponent = component;
        this.mcntx = mContext;
        this.bNotification = bNotification;
        this.mComponent = mComponent;
        this.mDataManager = mComponent.getClasesink();
        this.mPicasso = mComponent.getPicasso();
        this.mNetworkAccess = mComponent.getNetworkAccess();
    }

    public static BRFv4FragmentNofit newInstance(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, String bNotification) {
        return new BRFv4FragmentNofit(mCallback, component, mContext, bNotification);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgmrnt_camara, container, false);

        idVuew = (TextView) view.findViewById(R.id.idVuew);

        corect1 = (ImageView) view.findViewById(R.id.corect1);
        corect2 = (ImageView) view.findViewById(R.id.corect2);
        corect3 = (ImageView) view.findViewById(R.id.corect3);

        image1 = (ImageView) view.findViewById(R.id.image1);
        image2 = (ImageView) view.findViewById(R.id.image2);
        image3 = (ImageView) view.findViewById(R.id.image3);


        image2_face = (TextView) view.findViewById(R.id.image2_face);


        textView2 = (TextView) view.findViewById(R.id.textView2);
        textView = (TextView) view.findViewById(R.id.textView);

        camara_buttton = (Button) view.findViewById(R.id.camara_buttton);
        uplode_buttton = (Button) view.findViewById(R.id.uplode_buttton);
        button_tecimage = (Button) view.findViewById(R.id.button2);
        imageViewdd = (RelativeLayout) view.findViewById(R.id.imageViewdd);
        camara_view = (RelativeLayout) view.findViewById(R.id.camara_view);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        LinearLayout register_face = (LinearLayout) view.findViewById(R.id.register_face);
        camaraview = (FrameLayout) view.findViewById(R.id.camaraview);


        uplode_buttton.setVisibility(View.GONE);
        camara_view.setVisibility(View.INVISIBLE);
        camaraview.setVisibility(View.INVISIBLE);
        imageViewdd.setVisibility(View.VISIBLE);
        corect1.setVisibility(View.INVISIBLE);
        corect2.setVisibility(View.INVISIBLE);
        corect3.setVisibility(View.INVISIBLE);

        textView2.setText("Take the 3 image  face straight , face left  ,face right");
        textView.setText(R.string.welcome_for_n_hnb_face_registration);
        CallBackInterface.FaceRegisterbiometricLoginNotificationCallbackCallback mFaceRegisterbiometric = this;

        _camUtils = new CameraUtils();


        register_face.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        _example = new track_single_faceNotification(getActivity(), mComponent, mFaceRegisterbiometric, idVuew, _camUtils, bNotification);
        uplode_buttton.setVisibility(View.GONE);
        camara_view.setVisibility(View.VISIBLE);
        imageViewdd.setVisibility(View.GONE);
        button_tecimage.setVisibility(View.GONE);
        camaraview.setVisibility(View.VISIBLE);

        _preview = new AspectRatioTextureView(getActivity());
        _preview.setSurfaceTextureListener(_surfaceTextureListener);

        camaraview.addView(_preview);
        camaraview.addView(_example);

        camara_buttton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                uplode_buttton.setVisibility(View.GONE);
                camara_view.setVisibility(View.VISIBLE);
                imageViewdd.setVisibility(View.GONE);
                camaraview.setVisibility(View.VISIBLE);
            }
        });


        return view;
    }

    @Override
    public void textCallback(String text) {
        idVuew.setText(text);
    }


    private TextureView.SurfaceTextureListener _surfaceTextureListener =
            new TextureView.SurfaceTextureListener() {

                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {

                    if (_camUtils.init()) {

                        int adjWidth = 480;
                        int adjHeight = (int) (((double) height / (double) width) * adjWidth);
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        WindowManager windowmanager = (WindowManager) getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

                        _previewImageData = _preview.getBitmap(adjWidth, adjHeight);
                        updateLayout(_previewImageData.getWidth(), _previewImageData.getHeight());
                        _camUtils.startStream(surfaceTexture);

                    }
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                    _camUtils.stopStream();
                    return true;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
                    _preview.getBitmap(_previewImageData);
                }
            };

    private void updateLayout(int width, int height) {

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

        layoutParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = FrameLayout.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.CENTER;

        _preview.setAspectRatio(width, height);
        _preview.setLayoutParams(layoutParams);

        _example.init(width, height, _previewImageData);

        layoutParams = new FrameLayout.LayoutParams(width, height);

        layoutParams.width = width;
        layoutParams.height = height;
        layoutParams.gravity = Gravity.CENTER;

        _example.setLayoutParams(layoutParams);

        int viewWidth = camaraview.getWidth();
        int viewHeight = camaraview.getHeight();

        float viewRatio = viewWidth / viewHeight;
        float canvasRatio = width / height;
        float drawScale = 1.0f;

        if (viewRatio > canvasRatio) {
            drawScale = viewHeight / height;
        } else {
            drawScale = viewWidth / width;
        }

        _example.setScaleX(drawScale);
        _example.setScaleY(drawScale);
    }


    @Override
    public void onSuccessFaceRegisterbiometricCallback(boolean response) {

    }


    @Override
    public void onFailedFaceRegisterbiometricCallback(boolean message) {
        camara_view.setVisibility(View.GONE);
        camaraview.setVisibility(View.GONE);
        imageViewdd.setVisibility(View.VISIBLE);
//        uplode_buttton.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessRegisterbiometricCallback(RegisterBiometricResponse response) {
        mBiometricSucsess.onSuccessSeverLogingCallback(1);
    }

    @Override
    public void onFailedRegisterbiometricCallback(String message) {
        mBiometricSucsess.onFailedSeverLogingCallback(0);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("ResourceAsColor")
    public void detectFace(Bitmap imageData, ImageView image) {


        _example._brfManager.update(imageData);

        Vector<Rectangle> faces = _example._brfManager.getAllDetectedFaces();
        Log.d("faces.size()", "" + faces.size());
        if (faces.size() > 1) {
            button = true;

            image.setBackground(getResources().getDrawable(R.drawable.ic_action_incorect));

        } else {
            image.setBackground(getResources().getDrawable(R.drawable.ic_action_corect));

        }

        if (button) {
            uplode_buttton.setVisibility(View.VISIBLE);
            camara_buttton.setVisibility(View.GONE);
            textView2.setText("Multi Face Detection Please Try Again");
            textView2.setTextColor(R.color.Read);
        } else {

            uplode_buttton.setVisibility(View.GONE);
            camara_buttton.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onSuccessFaceRegisterbiometricLoginNotificationCallbackCallback(boolean response) {
        if (response) {
    getActivity().finish();
        }
    }

    @Override
    public void onFailedFaceRegisterbiometricLoginNotificationCallbackCallback(boolean message) {
        getActivity().finish();
    }
}
