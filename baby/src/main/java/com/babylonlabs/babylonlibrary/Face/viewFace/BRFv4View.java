package com.babylonlabs.babylonlibrary.Face.viewFace;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.TextureView;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.babylonlabs.babylonlibrary.Face.android.AspectRatioTextureView;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.Face.examples.BRFBasicJavaExample;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.*;

public class BRFv4View extends FrameLayout {

	private CameraUtils				_camUtils;
	private AspectRatioTextureView	_preview;
	private Bitmap					_previewImageData;
	private BRFBasicJavaExample		_example;
	private Constrect.userLogingStatesCallback mCallback ;

	public BRFv4View(Context context) {
		super(context);

	}
	public BRFv4View(Context context, Constrect.userLogingStatesCallback Callback) {
		super(context);
		mCallback = Callback;
	}

	public BRFv4View(Context context, AttributeSet attrs) {
		super(context, attrs);
	}



	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();

		_camUtils   = new CameraUtils();

		_preview	= new AspectRatioTextureView(getContext());
		_preview.setSurfaceTextureListener(_surfaceTextureListener);

//		_example = new track_single_face(getContext() ,_camUtils,mCallback);			// "basic - face tracking - track single face"			// "advanced - face tracking - face swap (two faces)"	- not implemented
//		_textView.setText();
		mCallback.textCallback("Blink 2 time Your eys");
		addView(_preview);
		addView(_example);
	}

	private TextureView.SurfaceTextureListener _surfaceTextureListener =
			new TextureView.SurfaceTextureListener() {

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {

				DisplayMetrics displayMetrics = new DisplayMetrics();
				int deviceWidth, deviceHeight;
				WindowManager windowmanager = (WindowManager) getContext().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
				windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
				deviceWidth = displayMetrics.widthPixels;
				deviceHeight = displayMetrics.heightPixels;
//				_previewImageData = _preview.getBitmap(deviceWidth, deviceHeight);
				_previewImageData = _preview.getBitmap(480, 854);

				updateLayout(_previewImageData.getWidth(), _previewImageData.getHeight());
//				updateLayout(deviceWidth, deviceHeight);


				_camUtils.startStream(surfaceTexture);

		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
			_camUtils.stopStream();
			return true;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
			_preview.getBitmap(_previewImageData);
		}
	};

	private void updateLayout(int width, int height) {

		Log.d("updateLayout", "w: " + width + " h: " + height);

		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

		layoutParams.width		= FrameLayout.LayoutParams.MATCH_PARENT;
		layoutParams.height		= FrameLayout.LayoutParams.MATCH_PARENT;
		layoutParams.gravity	= Gravity.CENTER;

		_preview.setAspectRatio(width, height);
		_preview.setLayoutParams(layoutParams);

		_example.init(width, height, _previewImageData);

		layoutParams			= new FrameLayout.LayoutParams(width, height);

		layoutParams.width		= width;
		layoutParams.height		= height;
		layoutParams.gravity	= Gravity.CENTER;

		_example.setLayoutParams(layoutParams);

		int viewWidth			= getWidth();
		int viewHeight			= getHeight();

		float viewRatio			= viewWidth / viewHeight;
		float canvasRatio		= width / height;
		float drawScale			= 1.0f;

		if(viewRatio > canvasRatio) {
			drawScale = viewHeight / height;
		} else {
			drawScale = viewWidth / width;
		}

		_example.setScaleX(drawScale);
		_example.setScaleY(drawScale);
	}

	public String Dhanushk(BRFBasicJavaExample example){

		return example.valu;
	}


}
