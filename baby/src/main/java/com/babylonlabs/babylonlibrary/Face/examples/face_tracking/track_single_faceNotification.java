package com.babylonlabs.babylonlibrary.Face.examples.face_tracking;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.Face.BRFFace;
import com.babylonlabs.babylonlibrary.Face.BRFManager;
import com.babylonlabs.babylonlibrary.Face.BRFMode;
import com.babylonlabs.babylonlibrary.Face.BRFState;
import com.babylonlabs.babylonlibrary.Face.BlinkCallback;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.Face.android.DrawingUtils;
import com.babylonlabs.babylonlibrary.Face.examples.BRFBasicJavaExample;
import com.babylonlabs.babylonlibrary.Face.geom.Point;
import com.babylonlabs.babylonlibrary.Face.geom.Rectangle;
import com.babylonlabs.babylonlibrary.Face.utils.FileUtils;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.network.CallApi;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Vector;

import javax.inject.Inject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by dhanushka on 2/21/18.
 */

public class track_single_faceNotification extends BRFBasicJavaExample implements BlinkCallback, CallBackInterface.FaceLoginbiometricLoginmCallbackNetworkNotificationCallback {
    private TextView mTextView;
    private CameraUtils cameraUtils;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    Context mContext;
    boolean sucsess = false;
    String bNotification;
    Bitmap mImageData;

    CallBackInterface.FaceRegisterbiometricLoginNotificationCallbackCallback mCallback;
    CallBackInterface.FaceLoginbiometricLoginmCallbackNetworkNotificationCallback mCallbackNetworkNotification;

    @Inject
    public CallApi mDataManager;

    @Inject
    public NetworkAccess mNetworkAccess;

    public track_single_faceNotification(Context context, ApplicationComponent mComponent, CallBackInterface.FaceRegisterbiometricLoginNotificationCallbackCallback mFaceRegisterbiometric, TextView viewText, CameraUtils mCameraUtils, String bNotification) {
        super(context);
        this.mTextView = viewText;
        this.cameraUtils = mCameraUtils;
        this.mContext = context;
        this.bNotification = bNotification;
        this.mCallback = mFaceRegisterbiometric;
        this.mCallbackNetworkNotification = (CallBackInterface.FaceLoginbiometricLoginmCallbackNetworkNotificationCallback) this;
        this.mDataManager = mComponent.getClasesink();
        this.mNetworkAccess = mComponent.getNetworkAccess();
    }

    double LeftEyeX;
    double RightEyeX;
    double eyeDist;
    int eyeYDist;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBlink(long eyeCloseDuration, long eyeOpenDuration) {
        Log.d("Blink", "Blinking Time:" + eyeCloseDuration + " Eyes openTime:" + eyeOpenDuration);
        if (sucsess) {
            getImage(mImageData);
//            cameraUtils.camera.takePicture(myShutterCallback, myPictureCallback_RAW, myPictureCallback_JPG);

        }

    }


    @Override
    public void initCurrentExample(BRFManager brfManager, Rectangle resolution) {

        Log.d("BRFv4", "BRFv4 - basic - face tracking - track single face\n" +
                "Detect and track one face and draw the 68 facial landmarks.");

        eyeDist = resolution.width * 0.25;
        eyeYDist = (int) (resolution.width * 0.6 + 1);

        RightEyeX = 0.375 * resolution.width + 1;
        LeftEyeX = RightEyeX + eyeDist;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getContext().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);


        brfManager.init(resolution, resolution, _appId);
        brfManager.setMode(BRFMode.POSE_DETECTION | BRFMode.BLINK_DETECTION);
        brfManager.setFaceDetectionParams(0.7, 0.8);
        brfManager.setBlinkDetectionParams(0.25, 0.18, this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void updateCurrentExample(BRFManager brfManager, Bitmap imageData, DrawingUtils draw) {

        brfManager.update(imageData);
        draw.clear();

        Vector<BRFFace> faces = brfManager.getFaces();

        if (faces.size() > 0) {
            sucsess = true;
            detecFace(faces, draw, imageData);
        } else {
            sucsess = false;
            mTextView.setTextColor(Color.RED);
            mTextView.setText("Can not detection your face");
        }

    }

    @SuppressLint("SetTextI18n")
    private void detecFace(Vector<BRFFace> faces, DrawingUtils draw, Bitmap imageData) {
        for (int i = 0; i < faces.size(); i++) {

            BRFFace face = faces.get(i);

            if (face.state == BRFState.FACE_TRACKING_START || face.state == BRFState.FACE_TRACKING) {

                draw.drawTriangles(face.vertices, face.triangles, false, 1.0, 0xffffff, 0.2);
                draw.drawVertices(face.vertices, 5.0, false, 0xffffff, 0.3);

                double imageDist = getEyeDist(getLeftEye(face.vertices), getRightEye(face.vertices));

                if (eyeDist - 12 < imageDist && imageDist < eyeDist + 12) {
                    mImageData = imageData;
                    mTextView.setText("Blink Your eys 1 time ");

                    sucsess = true;

                } else {
                    sucsess = false;
                    mTextView.setTextColor(Color.RED);
                    mTextView.setText("your face so far");
                }
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void severCall() {


        MultipartBody.Part bodyFaceImage = null;
        File file = new File(Environment.getExternalStoragePublicDirectory("image_sample"), "");
        File bitmapFile = new File(file, "FaceImage_send.jpg");
        Uri myUriFaceImage = Uri.parse("file:///" + bitmapFile.toString());

        bodyFaceImage = FileUtils.prepareFilePart("image", myUriFaceImage, mContext);

        RequestBody User_token = FileUtils.createPartFromString(Helpers.getUserToken(mContext));
        RequestBody User_bNotification = FileUtils.createPartFromString(bNotification);

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_token", User_token);
        map.put("channel", User_bNotification);

        mDataManager.AuthenticateFaceNotification(mNetworkAccess, map, 2, mCallbackNetworkNotification, bodyFaceImage);
        Helpers.showProgress(mContext);

    }


    private Bitmap convertColorIntoBlackAndWhiteImage(Bitmap orginal_Bitmap) {


        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(colorMatrix);
        Bitmap blackAndWhiteBitmap = orginal_Bitmap.copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixFilter);

        Matrix mat = new Matrix();
        mat.postRotate(-90);
        Bitmap bmpRotate = Bitmap.createBitmap(blackAndWhiteBitmap, 0, 0, blackAndWhiteBitmap.getWidth(), blackAndWhiteBitmap.getHeight(), mat, true);
        Canvas canvas = new Canvas(bmpRotate);
        canvas.drawBitmap(bmpRotate, 0, 0, paint);


        return bmpRotate;
    }


    private boolean storeImage(Bitmap b, String filename) {
        //get path to external storage (SD card)


        File bitmapFile = null;
        FileOutputStream fileOutputStream = null;
        File file = new File(Environment.getExternalStoragePublicDirectory("image_sample"), "");
        if (!file.exists()) {
            file.mkdir();
        }

        //create storage directories, if they don't exist
        try {
            double origWidth = b.getWidth();
            double origHeight = b.getHeight();

            final int destWidth = 480;//or the width you need

            if (origWidth > destWidth) {
                // picture is wider than we want it, we calculate its target height
                int destHeight = (int) ((origHeight / origWidth) * destWidth);

                // we create an scaled bitmap so it reduces the image, not just trim it
                Bitmap imageData = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
                bitmapFile = new File(file, filename + ".jpg");
                fileOutputStream = new FileOutputStream(bitmapFile);
                imageData.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }


    Point getRightEye(float[] vertices) {
        return new Point((vertices[36 * 2] + vertices[39 * 2]) / 2, (vertices[36 * 2 + 1] + vertices[39 * 2] + 1) / 2);
    }


    Point getLeftEye(float[] vertices) {
        return new Point((vertices[42 * 2] + vertices[45 * 2]) / 2, (vertices[42 * 2 + 1] + vertices[45 * 2] + 1) / 2);
    }

    double getEyeDist(Point left, Point right) {
        double d1 = left.x - right.x;
        double d2 = left.y - right.y;
        return Math.sqrt(d1 * d1 + d2 * d2);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void getImage(Bitmap bitmap) {
        Bitmap bitmapPicture = convertColorIntoBlackAndWhiteImage(bitmap);

        if (storeImage(bitmapPicture, "FaceImage_send")) {
            Helpers.showProgress(mContext);

            severCall();
        } else {
            mCallback.onFailedFaceRegisterbiometricLoginNotificationCallbackCallback(false);
        }

    }

    @Override


    public void onSuccessLoginbiometricLoginmCallbackNetworkNotificationCallback(boolean response) {
        Helpers.dismissProgress();
        mCallback.onSuccessFaceRegisterbiometricLoginNotificationCallbackCallback(true);
    }

    @Override
    public void onFailedFLoginbiometricLoginmCallbackNetworkNotificationCallback(boolean message) {
        Helpers.dismissProgress();
        mCallback.onFailedFaceRegisterbiometricLoginNotificationCallbackCallback(false);
    }
}
