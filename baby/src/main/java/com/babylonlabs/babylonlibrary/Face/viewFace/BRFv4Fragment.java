package com.babylonlabs.babylonlibrary.Face.viewFace;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.media.MediaActionSound;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.babylonlabs.babylonlibrary.Face.geom.Rectangle;
import com.babylonlabs.babylonlibrary.Face.utils.FileUtils;
import com.babylonlabs.babylonlibrary.R;
import com.babylonlabs.babylonlibrary.Face.android.AspectRatioTextureView;
import com.babylonlabs.babylonlibrary.Face.android.CameraUtils;
import com.babylonlabs.babylonlibrary.Face.examples.BRFBasicJavaExample;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.track_single_face;
import com.babylonlabs.babylonlibrary.Face.examples.face_tracking.track_single_face_register;
import com.babylonlabs.babylonlibrary.d.component.ApplicationComponent;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;
import com.babylonlabs.babylonlibrary.helpers.Helpers;
import com.babylonlabs.babylonlibrary.helpers.NetworkAccess;
import com.babylonlabs.babylonlibrary.helpers.RoundedCornersTransformation;
import com.babylonlabs.babylonlibrary.network.CallApi;
import com.babylonlabs.babylonlibrary.network.model.Request.RegisterBiometricRequest;
import com.babylonlabs.babylonlibrary.network.model.Response.RegisterBiometricResponse;
import com.babylonlabs.babylonlibrary.network.model.UserSetup;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Vector;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

@SuppressLint("ValidFragment")
@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BRFv4Fragment extends Fragment implements Constrect.userLogingStatesCallback, CallBackInterface.FaceRegisterbiometricCallback, CallBackInterface.RegisterbiometricCallback, CallBackInterface.FaceRegisterbiometricLoginCallback, CallBackInterface.RegisterbiometricFaceCallback {

    String text;
    TextView idVuew;
    FrameLayout camaraview;
    private CameraUtils _camUtils;
    private AspectRatioTextureView _preview;
    private Bitmap _previewImageData;
    private BRFBasicJavaExample _example;
    private TextView _textView,loginInPinButtton;
    ImageView image1, image2, image3, corect1, corect2, corect3,image_animation;
    TextView  textView2, textView;
    ImageView image2_face;
    private boolean login = true;
    RelativeLayout imageViewdd;
    CallBackInterface.biometricSucsessCallback mBiometricSucsess;
    LinearLayout linearLayout2;
    TextView desc;
    boolean newUser;
    boolean button;

    private Context mcntx;
    Button camara_buttton, button_tecimage, uplode_buttton;
    RelativeLayout camara_view;
    ApplicationComponent mComponent;
    Uri myUriFaceImage, myUrileftFaceImage, myUrirightFaceImage;
    File bitmapFile, leftFaceImage, rightFaceImage;
    @Inject
    public CallApi mDataManager;
    Picasso mPicasso;
    @Inject
    public NetworkAccess mNetworkAccess;

    @SuppressLint("ValidFragment")
    public BRFv4Fragment(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats) {
        this.mBiometricSucsess = mCallback;
        this.mComponent = component;
        this.mcntx = mContext;
        this.newUser = stats;
        this.mDataManager = mComponent.getClasesink();
        this.mPicasso = mComponent.getPicasso();
        this.mNetworkAccess = mComponent.getNetworkAccess();
    }

    public static BRFv4Fragment newInstance(CallBackInterface.biometricSucsessCallback mCallback, ApplicationComponent component, Context mContext, boolean stats) {
        return new BRFv4Fragment(mCallback, component, mContext, stats);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgmrnt_camara, container, false);

        idVuew = (TextView) view.findViewById(R.id.idVuew);

        corect1 = (ImageView) view.findViewById(R.id.corect1);
        corect2 = (ImageView) view.findViewById(R.id.corect2);
        corect3 = (ImageView) view.findViewById(R.id.corect3);

        image1 = (ImageView) view.findViewById(R.id.image1);
        image2 = (ImageView) view.findViewById(R.id.image2);
        image3 = (ImageView) view.findViewById(R.id.image3);


        image2_face = (ImageView) view.findViewById(R.id.image2_face);
        image_animation = (ImageView) view.findViewById(R.id.image_animation);


        textView2 = (TextView) view.findViewById(R.id.textView2);
        textView = (TextView) view.findViewById(R.id.textView);
        loginInPinButtton = (TextView) view.findViewById(R.id.login_in_pin_buttton);

        camara_buttton = (Button) view.findViewById(R.id.camara_buttton);
        uplode_buttton = (Button) view.findViewById(R.id.uplode_buttton);
        button_tecimage = (Button) view.findViewById(R.id.button2);
        imageViewdd = (RelativeLayout) view.findViewById(R.id.imageViewdd);
        camara_view = (RelativeLayout) view.findViewById(R.id.camara_view);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        LinearLayout register_face = (LinearLayout) view.findViewById(R.id.register_face);
         linearLayout2 = (LinearLayout) view.findViewById(R.id.linearLayout2);
        camaraview = (FrameLayout) view.findViewById(R.id.camaraview);
        Glide.with(getActivity())
                .load(R.drawable.camata_tu)
                .into(image_animation);



        uplode_buttton.setVisibility(View.GONE);
        camara_view.setVisibility(View.INVISIBLE);
        camaraview.setVisibility(View.INVISIBLE);
        imageViewdd.setVisibility(View.VISIBLE);
        corect1.setVisibility(View.INVISIBLE);
        corect2.setVisibility(View.INVISIBLE);
        corect3.setVisibility(View.INVISIBLE);

        textView.setText(R.string.welcome_for_n_hnb_face_registration);
        CallBackInterface.FaceRegisterbiometricCallback mFaceRegisterbiometric = this;
        CallBackInterface.FaceRegisterbiometricLoginCallback mFaceRegisterbiometricLogin = this;
        _camUtils = new CameraUtils();

        loginInPinButtton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBiometricSucsess.onSuccessSeverPINingCallback(1);
            }
        });
        if (newUser) {
            linearLayout2.setVisibility(View.GONE);
            image_animation.setVisibility(View.VISIBLE);
            register_face.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            loginInPinButtton.setVisibility(View.VISIBLE);
            _example = new track_single_face(getActivity(), mComponent, mFaceRegisterbiometricLogin, idVuew, _camUtils);
            uplode_buttton.setVisibility(View.GONE);
            camara_view.setVisibility(View.VISIBLE);
            imageViewdd.setVisibility(View.GONE);
            button_tecimage.setVisibility(View.GONE);
            camaraview.setVisibility(View.VISIBLE);

        } else {

            Glide.with(getActivity())
                    .load(R.drawable.frontface)
                    .into(image2_face);
            uplode_buttton.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.GONE);
            image_animation.setVisibility(View.VISIBLE);
            camara_view.setVisibility(View.INVISIBLE);
            camaraview.setVisibility(View.INVISIBLE);
            imageViewdd.setVisibility(View.VISIBLE);
            corect1.setVisibility(View.INVISIBLE);
            corect2.setVisibility(View.INVISIBLE);
            corect3.setVisibility(View.INVISIBLE);
            register_face.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.INVISIBLE);
            loginInPinButtton.setVisibility(View.GONE);
            _example = new track_single_face_register(getActivity(), this);
        }
        _preview = new AspectRatioTextureView(getActivity());
        _preview.setSurfaceTextureListener(_surfaceTextureListener);

        camaraview.addView(_preview);
        camaraview.addView(_example);

        camara_buttton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                uplode_buttton.setVisibility(View.GONE);
                camara_view.setVisibility(View.VISIBLE);
                imageViewdd.setVisibility(View.GONE);
                camaraview.setVisibility(View.VISIBLE);
            }
        });

        uplode_buttton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                severCall();
            }
        });


        return view;
    }

    @Override
    public void textCallback(String text) {
        idVuew.setText(text);
    }


    private TextureView.SurfaceTextureListener _surfaceTextureListener =
            new TextureView.SurfaceTextureListener() {

                @Override
                public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {

                    if (_camUtils.init()) {

                        int adjWidth = 480;
                        int adjHeight = (int) (((double) height / (double) width) * adjWidth);
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        WindowManager windowmanager = (WindowManager) getActivity().getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
                        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);

                        _previewImageData = _preview.getBitmap(adjWidth, adjHeight);
                        updateLayout(_previewImageData.getWidth(), _previewImageData.getHeight());
                        _camUtils.startStream(surfaceTexture);

                    }
                }

                @Override
                public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
                }

                @Override
                public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                    _camUtils.stopStream();
                    return true;
                }

                @Override
                public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
                    _preview.getBitmap(_previewImageData);
                }
            };

    private void updateLayout(int width, int height) {

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(width, height);

        layoutParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = FrameLayout.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.CENTER;

        _preview.setAspectRatio(width, height);
        _preview.setLayoutParams(layoutParams);

        _example.init(width, height, _previewImageData);

        layoutParams = new FrameLayout.LayoutParams(width, height);

        layoutParams.width = width;
        layoutParams.height = height;
        layoutParams.gravity = Gravity.CENTER;

        _example.setLayoutParams(layoutParams);

        int viewWidth = camaraview.getWidth();
        int viewHeight = camaraview.getHeight();

        float viewRatio = viewWidth / viewHeight;
        float canvasRatio = width / height;
        float drawScale = 1.0f;

        if (viewRatio > canvasRatio) {
            drawScale = viewHeight / height;
        } else {
            drawScale = viewWidth / width;
        }

        _example.setScaleX(drawScale);
        _example.setScaleY(drawScale);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void severCall() {

        Gson gson = new Gson();

        UserSetup mUserSetup = gson.fromJson(Helpers.getUser(getActivity()), UserSetup.class);
        mUserSetup.getDeviceId();

        MultipartBody.Part bodyFaceImage = null;
        MultipartBody.Part bodyLeftFaceImage = null;
        MultipartBody.Part bodyRightFaceImage = null;

        File file = new File(Environment.getExternalStoragePublicDirectory("image_sample"), "");
        bitmapFile = new File(file, "FaceImage.jpg");
        leftFaceImage = new File(file, "leftFaceImage.jpg");
        rightFaceImage = new File(file, "rightFaceImage.jpg");


        bitmapFile = new File(bitmapFile.toString());
        leftFaceImage = new File(leftFaceImage.toString());
        rightFaceImage = new File(rightFaceImage.toString());

        RequestBody requestFile_nl = RequestBody.create(MediaType.parse("image/*"), bitmapFile);
        RequestBody requestFile_lt = RequestBody.create(MediaType.parse("image/*"), leftFaceImage);
        RequestBody requestFile_rt = RequestBody.create(MediaType.parse("image/*"), rightFaceImage);

        bodyFaceImage = MultipartBody.Part.createFormData("image_nl", file.getName(), requestFile_nl);
        bodyLeftFaceImage = MultipartBody.Part.createFormData("image_lt", file.getName(), requestFile_lt);
        bodyRightFaceImage = MultipartBody.Part.createFormData("image_rt", file.getName(), requestFile_rt);


        RegisterBiometricRequest mRegisterBiometricRequest = new RegisterBiometricRequest();
        mRegisterBiometricRequest.setDevice_key(mUserSetup.getDeviceId());
        mRegisterBiometricRequest.setDevice_type("Android");
        mRegisterBiometricRequest.setLocation("all");
        mRegisterBiometricRequest.setProduct_ids("1");
        mRegisterBiometricRequest.setPush_token(mUserSetup.getUserToken());
        mRegisterBiometricRequest.setUser_token(Helpers.getUserToken(getActivity()));

        RequestBody Device_key = FileUtils.createPartFromString(mRegisterBiometricRequest.getDevice_key());
        RequestBody Device_type = FileUtils.createPartFromString(mRegisterBiometricRequest.getDevice_type());
        RequestBody Location = FileUtils.createPartFromString(mRegisterBiometricRequest.getLocation());
        RequestBody Product_ids = FileUtils.createPartFromString(mRegisterBiometricRequest.getProduct_ids());
        RequestBody Push_token = FileUtils.createPartFromString(mRegisterBiometricRequest.getPush_token());
        RequestBody User_token = FileUtils.createPartFromString(mRegisterBiometricRequest.getUser_token());

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_token", User_token);
        map.put("device_key", Device_key);
        map.put("push_token", Push_token);
        map.put("device_type", Device_type);
        map.put("product_ids", Product_ids);

        Helpers.showProgress(getActivity());
        mDataManager.registerbiometricFace(this, mNetworkAccess, 2, map, bodyFaceImage, bodyLeftFaceImage, bodyRightFaceImage);
    }


    @Override
    public void onSuccessFaceRegisterbiometricCallback(boolean response) {

    }

    @SuppressLint("ResourceAsColor")
    private void registtioncallBack() {
        textView2.setText("");

        File file = new File(Environment.getExternalStoragePublicDirectory("image_sample"), "");
        bitmapFile = new File(file, "FaceImage.jpg");
        leftFaceImage = new File(file, "leftFaceImage.jpg");
        rightFaceImage = new File(file, "rightFaceImage.jpg");

        myUriFaceImage = Uri.parse("file:///" + bitmapFile.toString());
        myUrileftFaceImage = Uri.parse("file:///" + leftFaceImage.toString());
        myUrirightFaceImage = Uri.parse("file:///" + rightFaceImage.toString());


        Bitmap bitmap = BitmapFactory.decodeFile(bitmapFile.getPath());
        Bitmap bitmap2 = BitmapFactory.decodeFile(myUrileftFaceImage.getPath());
        Bitmap bitmap3 = BitmapFactory.decodeFile(myUrirightFaceImage.getPath());


        loadImage(getActivity(), myUriFaceImage.toString(), image2);
        loadImage(getActivity(), myUrileftFaceImage.toString(), image1);
        loadImage(getActivity(), myUrirightFaceImage.toString(), image3);

        _example._brfManager.setFaceDetectionParams(0.1, 0.8);
        _example._brfManager.setNumFacesToTrack(2);
        button = false;
        detectFace(bitmap, corect2);
        detectFace(bitmap2, corect1);
        detectFace(bitmap3, corect3);


        if (button) {
            uplode_buttton.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.GONE);
            textView.setVisibility(View.INVISIBLE);
            camara_buttton.setVisibility(View.VISIBLE);
            textView2.setText("Face Detection Please Try Again");
        } else {

            textView2.setText("Upload Images");
            uplode_buttton.setVisibility(View.VISIBLE);
            camara_buttton.setVisibility(View.GONE);
            linearLayout2.setVisibility(View.VISIBLE);
            textView.setVisibility(View.
                    INVISIBLE);
            image_animation.setVisibility(View.INVISIBLE);
        }
        _example._brfManager.setFaceDetectionParams(0.7, 0.8);

        corect1.setVisibility(View.VISIBLE);
        corect2.setVisibility(View.VISIBLE);
        corect3.setVisibility(View.VISIBLE);
        camara_view.setVisibility(View.GONE);
        camaraview.setVisibility(View.GONE);
        imageViewdd.setVisibility(View.VISIBLE);
    }


    @Override
    public void onFailedFaceRegisterbiometricCallback(boolean message) {
        camara_view.setVisibility(View.GONE);
        camaraview.setVisibility(View.GONE);
        imageViewdd.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSuccessRegisterbiometricCallback(RegisterBiometricResponse response) {
        Helpers.dismissProgress();
        mBiometricSucsess.onSuccessSeverPINingCallback(1);
    }

    @Override
    public void onFailedRegisterbiometricCallback(String message) {
        Helpers.dismissProgress();
        mBiometricSucsess.onFailedSeverLogingCallback(0);
    }

    public void loadImage(final Context context, String imageURL, final ImageView imageView) {
        if (imageURL != "") {
            mPicasso.load(imageURL)
                    .resize(110, 110)
                    .transform(new RoundedCornersTransformation(8, 2, RoundedCornersTransformation.CornerType.ALL))
                    .centerCrop()
                    .into(imageView);
        }
    }

    @Override
    public void onSuccessFaceRegisterbiometricLoginCallback(boolean response) {

        mBiometricSucsess.onSuccessSeverLogingCallback(1);
    }

    @Override
    public void onFailedFaceRegisterbiometricLoginCallback(boolean message) {
        mBiometricSucsess.onFailedSeverLogingCallback(0);
    }


    @SuppressLint("ResourceAsColor")
    public void detectFace(Bitmap imageData, ImageView image) {

        _example._brfManager.update(imageData);
        Vector<Rectangle> faces = _example._brfManager.getAllDetectedFaces();

        if (faces.size() > 1) {
            button = true;

            image.setBackground(getResources().getDrawable(R.drawable.ic_action_incorect));

        } else {
            image.setBackground(getResources().getDrawable(R.drawable.ic_action_corect));

        }
    }

    @Override
    public void onSuccessRegisterbiometricFaceCallback(int cod, String message) {

        if (cod == 1) {
            idVuew.setTextColor(Color.parseColor("#FFFF0000"));
            button_tecimage.setVisibility(View.GONE);
            idVuew.setText(text);
        } else {
            idVuew.setTextColor(Color.parseColor("#FFFF0000"));
            idVuew.setText(message);
            button_tecimage.setVisibility(View.GONE);
        }


    }

    @Override
    public void onFailedRegisterbiometricFaceCallback(String message) {

        button_tecimage.setVisibility(View.GONE);
        idVuew.setTextColor(Color.parseColor("#FFFF0000"));
        idVuew.setText(message);
    }

    @Override
    public void onCaptureImageFaceCallback(String Buttonmessage, final int testmesseg, final String imageName, final Bitmap mBitmap) {

        button_tecimage.setVisibility(View.VISIBLE);
//        button_tecimage.setText(Buttonmessage);
        idVuew.setTextColor(Color.parseColor("#ffffff"));
        button_tecimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaActionSound m = null;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    m = new MediaActionSound();
                    m.play(MediaActionSound.SHUTTER_CLICK);
                }

                if (testmesseg == 1) {

                    BRFBasicJavaExample.funtFaceImage = true;
                    Glide.with(getActivity())
                            .load(R.drawable.fr)
                            .into(image2_face);
                    text = "Adjust your face right";
                    idVuew.setText(text);
                    takeImage(mBitmap, imageName);

                } else if (testmesseg == 2) {

                    BRFBasicJavaExample.leftFaceImage = true;
                    Glide.with(getActivity())
                            .load(R.drawable.fl)
                            .into(image2_face);
                    text = "Adjust your face left";
                    idVuew.setText(text);
                    takeImage(mBitmap, imageName);

                } else if (testmesseg == 3){

                    BRFBasicJavaExample.rightFaceImage = true;
                    text = "Thanks";
                    idVuew.setText(text);

                    if(takeImage(mBitmap, imageName)) {
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                registtioncallBack();
                            }
                        }, 800);

                    }
                }
            }
        });
    }


    private boolean takeImage(Bitmap bitmap, String name) {
        Bitmap bitmapPicture = convertColorIntoBlackAndWhiteImage(bitmap);
        return storeImage(bitmapPicture, name);

    }


    private Bitmap convertColorIntoBlackAndWhiteImage(Bitmap orginal_Bitmap) {

        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(colorMatrix);
        Bitmap blackAndWhiteBitmap = orginal_Bitmap.copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixFilter);

        Canvas canvas = new Canvas(blackAndWhiteBitmap);
        canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);

        return blackAndWhiteBitmap;
    }


    private boolean storeImage(Bitmap b, String filename) {

        File bitmapFile = null;
        FileOutputStream fileOutputStream = null;
        File file = new File(Environment.getExternalStoragePublicDirectory("image_sample"), "");
        if (!file.exists()) {
            file.mkdir();
        }

        try {
            double origWidth = b.getWidth();
            double origHeight = b.getHeight();

            final int destWidth = 480;//or the width you need

            if (origWidth > destWidth) {

                int destHeight = (int) ((origHeight / origWidth) * destWidth);
                Bitmap imageData = Bitmap.createScaledBitmap(b, destWidth, destHeight, false);
                bitmapFile = new File(file, filename + ".jpg");
                fileOutputStream = new FileOutputStream(bitmapFile);
                imageData.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

            } else {
                bitmapFile = new File(file, filename + ".jpg");
                fileOutputStream = new FileOutputStream(bitmapFile);
                b.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            }


        } catch (FileNotFoundException e) {
            Log.w("TAG", "Error saving image file: " + e.getMessage());
            return false;
        }

        return true;
    }




}