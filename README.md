Android Babylon API guideline

1) create BaseActivity.java file and copy and paste this cord line hear

    protected ApplicationComponent componentBabylone; 
    
    //this is dagger 2 component Network Call  with retrofit 
    
      componentBabylone = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
                
2) Call this function your splash screen and pass user notification key token  

//before call this function you should check GPS availability. of GPS not available you should notify it and enbrel GPS location .
we will get user Device ID, push token, location 

     SetupClass.newInstance(context, "token");
     
3) extends your Baseactivity to your login activity

      public class MainActivity extends BaseActivity
     
4) Now this function implements your Login Activity

      public class MainActivity extends BaseActivity 
                   implements CallBackInterface.userLogingStatesCallback {
                   
5) copy pase this cord your Login Activity   
 
     public class MainActivity extends BaseActivity 
                   implements CallBackInterface.userLogingStatesCallback {
     
    CallBackInterface.userLogingStatesCallback mLogingStatesCallback = this;
    final CallBackInterface.userLogingStatesCallback mStatesCallback = this;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ....
        }
        
        
          @Override
    public void onSuccessuserLogingStatesCallback(int response, boolean logingstets) {
      ...
    }

    @Override
    public void onSuccessBiometricsCompletedCallback(String response) {
     ...
    }

    @Override
    public void onFaileduserLogingStatesCallback(String message) {
       ...
    }
    
6) Now after enter password and username and check user login success or no .
   if your success call this method
   
        //you should parse user 
          * Context 
          * login token 
          * component
          * CallBack 
          
    MainAuthenticateClass.newInstance(MainActivity.this, Usertoken, componentBabylone, mStatesCallback);

7) after we will send you Biometrics status and you should copy paste this code

       @Override
    public void onSuccessuserLogingStatesCallback(int response, boolean logingstets) {
      
    Intent i = new Intent(this, BiometricsActivity.class);
    
    // BIOMETRICS_TYPE (Fingerprint = 1,Signature = 2,Face = 0,)
        i.putExtra("BIOMETRICS_TYPE", response);
        i.putExtra("LOGIN_STATS", logingstets);
        i.putExtra("NIC", UserNic);
        startActivityForResult(i, 2);
    }

    @Override
    public void onSuccessBiometricsCompletedCallback(String response) {
        //loging scusess
       
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        Toast.makeText(this, "loging scusess",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFaileduserLogingStatesCallback(String message) {
    
        Toast.makeText(this, "loging Failed",Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2

        if (requestCode == 2) {

            int message = data.getIntExtra("MESSAGE", -1);
            if (message == 1) {

                mLogingStatesCallback.onSuccessBiometricsCompletedCallback("go hame");
            } else {

                mLogingStatesCallback.onFaileduserLogingStatesCallback("login fail");
            }
        }
    }
     
     
     
     
8) Create new Activity "BiometricsActivity" and copy and pase this cord 

        import com.babylonlabs.babylonlibrary.ViewClass.Fragment.FingerprintFragment;
        import com.babylonlabs.babylonlibrary.helpers.BabylonConstant;
        import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;

         public class BiometricsActivity extends BaseActivity implements CallBackInterface.biometricSucsessCallback, CallBackInterface.biometricPINSucsessCallback {

    boolean stats;
    boolean notification;
    boolean push = true;
    Context mContext = this;

    private static final String TAG = "CameraPermissionActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biometrics);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Biometrics");

        int s = getIntent().getIntExtra("BIOMETRICS_TYPE", -1);
        stats = getIntent().getBooleanExtra("LOGIN_STATS", false);
        UserNic = getIntent().getStringExtra("NIC");


        if (null == savedInstanceState) {

          if (s == 1) {
                    setFragmentClase(FingerprintFragmentNew.newInstance(this, componentBabylone, mContext, false, UserNic));
                } else if (s == 0) {
                    checkCamaraste(false);
                } else {
                    setFragmentClase(SignatureFragment.newInstance(mbiometricSucsessCallback, componentBabylone, mContext, false, UserNic));
                }


        }
    }

    private void setFragmentClase(Fragment fingerprintFragment) {

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fingerprintFragment)
                .commit();
    }

    @Override
    public void onSuccessSeverLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onFailedSeverLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onSuccessSeverPINingCallback(int message) {
        setFragmentClase(PinFragment.PinFragmentint(this, componentBabylone, mContext, stats));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", 2);
        setResult(2, intent);
        finish();
    }


    @SuppressLint("NewApi")
    protected void checkCamaraste() {

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {

        }
        setFragmentClase(BRFv4Fragment.newInstance(this, componentBabylone, mContext, stats));
    }


    @Override
    public void onSuccessSeverPINLogingCallback(int response) {
        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onFailedSeverPINLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }
}






xml File "activity_biometrics.xml"

       <?xml version="1.0" encoding="utf-8"?>
       <FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
       xmlns:tools="http://schemas.android.com/tools"
       android:id="@+id/container"
       android:layout_width="match_parent"
       android:layout_height="match_parent"
       android:background="#000"
       tools:context=".BiometricsActivity" />




     