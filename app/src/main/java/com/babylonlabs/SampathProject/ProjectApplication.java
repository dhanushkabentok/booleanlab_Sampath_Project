package com.babylonlabs.SampathProject;

import android.app.Activity;
import android.app.Application;


import com.babylonlabs.SampathProject.network.ApiService;
import com.babylonlabs.SampathProject.d.component.ApplicationComponentSampathBank;
import com.babylonlabs.SampathProject.d.component.DaggerApplicationComponentSampathBank;
import com.babylonlabs.SampathProject.d.module.ContextModule;
import com.babylonlabs.SampathProject.helpers.NetworkAccess;
import com.babylonlabs.SampathProject.network.CallApi;
import com.squareup.picasso.Picasso;

/**
 * Created by dhanushka on 10/10/2017.
 */

public class ProjectApplication extends Application {
    private ApplicationComponentSampathBank component;
    private ApiService apiService;
    private Picasso picasso;
    private CallApi mCallApi;
    private NetworkAccess mNetworkAccess;

    public static ProjectApplication get(Activity activity) {
        return (ProjectApplication) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponentSampathBank.builder()
                .contextModule(new ContextModule(this))
                .build();

        apiService = component.getApplicatonService();
        picasso = component.getPicasso();
        mCallApi = component.getClasesink();
        mNetworkAccess = component.getNetworkAccess();
    }

    public ApplicationComponentSampathBank getcomponent() {
        return component;
    }
}
