package com.babylonlabs.SampathProject.d.component;




import com.babylonlabs.SampathProject.BaseActivity;
import com.babylonlabs.SampathProject.d.PerActivity;
import com.babylonlabs.SampathProject.d.module.ActivityModule;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponentSampathBank.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}
