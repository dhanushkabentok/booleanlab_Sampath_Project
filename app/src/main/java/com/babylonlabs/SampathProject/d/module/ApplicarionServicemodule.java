package com.babylonlabs.SampathProject.d.module;


import com.babylonlabs.SampathProject.d.ApplicationScope;
import com.babylonlabs.SampathProject.network.ApiService;
import com.fatboyindustrial.gsonjodatime.DateTimeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhanushka on 10/10/2017.
 */
@Module(includes = NetworkModule.class)
public class ApplicarionServicemodule {

    @Provides
    @ApplicationScope
    public ApiService apiService(Retrofit applicatioRetrofit) {

        return applicatioRetrofit.create(ApiService.class);
    }

    @Provides
    @ApplicationScope
    public Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        return gsonBuilder.create();
    }

    @Provides
    @ApplicationScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl("https://babylon4c.bentok.com/bank/")
//                .baseUrl("http://192.168.8.100:8000/bank/")
                .build();
    }


}
