package com.babylonlabs.SampathProject.d.component;


import com.babylonlabs.SampathProject.d.ApplicationScope;
import com.babylonlabs.SampathProject.d.module.NetworkAccessModule;
import com.babylonlabs.SampathProject.d.module.SicModul;
import com.babylonlabs.SampathProject.network.ApiService;
import com.babylonlabs.SampathProject.network.CallApi;
import com.babylonlabs.SampathProject.d.module.ActivityModule;
import com.babylonlabs.SampathProject.d.module.ApplicarionServicemodule;
import com.babylonlabs.SampathProject.d.module.PicassoModule;
import com.babylonlabs.SampathProject.helpers.NetworkAccess;
import com.squareup.picasso.Picasso;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@ApplicationScope
@Component(modules = {ApplicarionServicemodule.class, PicassoModule.class, ActivityModule.class, SicModul.class, NetworkAccessModule.class})
public interface ApplicationComponentSampathBank {

    Picasso getPicasso();
    ApiService getApplicatonService();
    CallApi getClasesink();
    NetworkAccess getNetworkAccess();
}
