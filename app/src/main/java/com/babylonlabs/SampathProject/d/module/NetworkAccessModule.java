package com.babylonlabs.SampathProject.d.module;

import android.content.Context;


import com.babylonlabs.SampathProject.d.ApplicationScope;
import com.babylonlabs.SampathProject.d.ApplicationContext;
import com.babylonlabs.SampathProject.helpers.NetworkAccess;

import dagger.Module;
import dagger.Provides;
//2705
/**
 * Created by dhanushka on 24/10/2017.
 */
@Module(includes = {ContextModule.class})
public class NetworkAccessModule {

    @Provides
    @ApplicationScope
    public NetworkAccess mNetworkAccess(@ApplicationContext Context context) {
        return new NetworkAccess(context);
    }
}
