package com.babylonlabs.SampathProject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.babylonlabs.babylonlibrary.Face.viewFace.BRFv4Fragment;
import com.babylonlabs.babylonlibrary.ViewClass.Fragment.FingerprintFragment;
import com.babylonlabs.babylonlibrary.ViewClass.Fragment.PinFragment;
import com.babylonlabs.babylonlibrary.helpers.BabylonConstant;
import com.babylonlabs.babylonlibrary.helpers.CallBackInterface;

public class BiometricsActivity extends BaseActivity implements CallBackInterface.biometricSucsessCallback, CallBackInterface.biometricPINSucsessCallback {

    boolean stats;
    boolean notification;
    boolean push = true;
    Context mContext = this;

    private static final String TAG = "CameraPermissionActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biometrics);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Biometrics");

        int s = getIntent().getIntExtra("BIOMETRICS_TYPE", -1);
        stats = getIntent().getBooleanExtra("LOGIN_STATS", false);
        notification = getIntent().getBooleanExtra("NOTIFICATION", false);


        if (null == savedInstanceState) {

            if (s == 1) {

                setFragmentClase(FingerprintFragment.newInstance(this, componentBabylone, mContext, stats, notification));
            } else {
                //Face
                checkCamaraste();
            }


        }
    }

    private void setFragmentClase(Fragment fingerprintFragment) {

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fingerprintFragment)
                .commit();
    }

    @Override
    public void onSuccessSeverLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onFailedSeverLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onSuccessSeverPINingCallback(int message) {
        setFragmentClase(PinFragment.PinFragmentint(this, componentBabylone, mContext, stats));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", 2);
        setResult(2, intent);
        finish();
    }


    @SuppressLint("NewApi")
    protected void checkCamaraste() {

        try {
            getSupportActionBar().hide();
        } catch (NullPointerException e) {

        }
        setFragmentClase(BRFv4Fragment.newInstance(this, componentBabylone, mContext, stats));
    }


    @Override
    public void onSuccessSeverPINLogingCallback(int response) {
        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }

    @Override
    public void onFailedSeverPINLogingCallback(int response) {

        Intent intent = new Intent();
        intent.putExtra("MESSAGE", response);
        setResult(2, intent);
        finish();
    }
}


