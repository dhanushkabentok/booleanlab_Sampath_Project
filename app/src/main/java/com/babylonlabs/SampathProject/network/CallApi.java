package com.babylonlabs.SampathProject.network;

import android.content.Context;
import android.util.Log;


import com.babylonlabs.SampathProject.network.model.User;
import com.babylonlabs.SampathProject.helpers.NetworkAccess;
import com.babylonlabs.SampathProject.network.model.GithubRepo;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dhanushka on 19/10/2017.
 */

public class CallApi {

    @Inject
    ApiService mSicModul;
    private Context mContext;



    public CallApi(Context context, ApiService m) {
        mContext = context;
        mSicModul = m;
    }

    public void getServercal(final GetSDPListCallback callback, User mUser) {

        if (!NetworkAccess.isNetworkAvailable()) {
            callback.onFailedGetSDPList(null);
            return;
        }



        mSicModul.getAllRepos(mUser).enqueue(new Callback<GithubRepo>() {
            @Override
            public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                Log.d("rgfgfg", "gfgghrtgrthw");
                if (response.isSuccessful()) {
                            callback.onSuccessGetSDPList(response.body().getUser_token());
                }

            }

            @Override
            public void onFailure(Call<GithubRepo> call, Throwable t) {
                callback.onFailedGetSDPList(t.getMessage());
            }
        });}


    public void register(final GetSDPListCallbackReg callback, User mUser) {

        if (!NetworkAccess.isNetworkAvailable()) {
            callback.onFailedGetSDPListCallbackReg(null);
            return;
        }


        mSicModul.create_user(mUser).enqueue(new Callback<GithubRepo>() {
            @Override
            public void onResponse(Call<GithubRepo> call, Response<GithubRepo> response) {
                Log.d("rgfgfg", "gfgghrtgrthw");
                callback.onSuccessGetSDPListCallbackReg("susses");

            }

            @Override
            public void onFailure(Call<GithubRepo> call, Throwable t) {
                callback.onFailedGetSDPListCallbackReg(t.getMessage());
            }
        });
    }

    public interface GetSDPListCallback {
        void onSuccessGetSDPList(String sdpList);

        void onFailedGetSDPList(String message);
    }

    public interface GetSDPListCallbackReg {
        void onSuccessGetSDPListCallbackReg(String sdpList);

        void onFailedGetSDPListCallbackReg(String message);
    }
}
