
package com.babylonlabs.SampathProject.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

public class GithubRepo {

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    @SerializedName("user_token")
    @Expose
    public String user_token;

}
