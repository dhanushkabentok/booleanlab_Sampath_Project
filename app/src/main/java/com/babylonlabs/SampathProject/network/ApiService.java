package com.babylonlabs.SampathProject.network;

import com.babylonlabs.SampathProject.network.model.GithubRepo;
import com.babylonlabs.SampathProject.network.model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by dhanushka on 10/10/2017.
 */

public interface ApiService {


    @POST("get_user_token/")
    Call<GithubRepo> getAllRepos(@Body User mUser);

    @POST("create_user/")
    Call<GithubRepo> create_user(@Body User mUser);


}
