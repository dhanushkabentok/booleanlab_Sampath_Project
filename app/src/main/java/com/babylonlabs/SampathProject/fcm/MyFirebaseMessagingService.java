package com.babylonlabs.SampathProject.fcm;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;


import com.babylonlabs.SampathProject.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;


/**
 * Created by nisala on 9/24/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private String mStringType;



    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        handleNotification(remoteMessage);
    }

    private void handleNotification(RemoteMessage remoteMessage) {

//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //Check if message contains data
//        if (extras.getString("body") != null && extras.getString("title") != null && extras.getString("request_id") != null && extras.getString("type") != null) {
//            if (isAppInBackground(this)) {
//                int type = Integer.parseInt(extras.getString("type"));
//                sendNotification(extras.getString("title"), extras.getString("body"), type);
//
//            }
//        }

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob();
            } else {
                // Handle message within 10 seconds
//                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    //Display the notification
    private void sendNotification(String title, String message, int type) {
//        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
//
//        intent.putExtra(MyGarageConstant.BUNDLE_FROM_FIREBASE, 1);
//        intent.putExtra(MyGarageConstant.BUNDLE_PUSH_TYPE, type);
//
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        final PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);


//        //Set sound of notification
//        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        final android.support.v4.app.NotificationCompat.Builder mBuilder = new android.support.v4.app.NotificationCompat.Builder(this)
//                .setContentTitle(title)
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setSound(notificationSound)
////                .setContentIntent(resultPendingIntent)
//                .setSmallIcon(R.drawable.ic_action_corect)
//
//                .setPriority(android.support.v7.app.NotificationCompat.PRIORITY_HIGH);

//        NotificationManager notificationManager = (NotificationManager) .getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0, mBuilder.build());
    }

    public boolean isAppInBackground(MyFirebaseMessagingService context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }
}